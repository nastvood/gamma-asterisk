<?php

class JsonextController extends Controller{
    public $layout='//layouts/json';

    public $asteriskApps=array('Answer','Background','Dial','Goto','Gotoif','Hangup','NoOp','Playback','Set','SetMusicOnHold','VoiceMail','VoiceMailMain','WaitExten');

    public function actionContexts(){
        $modelContexts=Contexts::model()->findAll();

        $contexts=array();
        foreach($modelContexts as $modelContext){
            array_push(
                $contexts,
                array(
                    'idContext'=>$modelContext->id_context,
                    'context'=>$modelContext->context,
                    'comment'=>$modelContext->comment
                )
            );
        }

        $json=CJSON::encode($contexts);

        $this->render('json',array('json'=>$json));
    }

    public function actionContext($context){
        $model=Contexts::model()->with(array('exts','exts.apps'))->findByPk($context);

        $exts=array();

        if ($model){
            foreach ($model->exts as $modelExt){
                $apps=array();
                foreach ($modelExt->apps as $modelApp){
                    array_push(
                        $apps,
                        array(
                            'idApp'=>$modelApp->id_app,
                            'ext'=>$modelApp->ext,
                            'app'=>$modelApp->app,
                            'priority'=>$modelApp->priority,
                            'appdata'=>$modelApp->appdata
                        )
                    );
                }

                array_push(
                    $exts,
                    array(
                        'idExt'=>$modelExt->id_ext,
                        'ext'=>$modelExt->ext,
                        'idContext'=>$modelExt->context,
                        'apps'=>$apps
                    )
                );
            }
        }

        $json=CJSON::encode($exts);

        $this->render('json',array('json'=>$json));
    }

    public function actionEditContext($context){
        $model=Contexts::model()->findByPk($context);

        $result=new JsonResult();

        if($model){
            $this->performAjaxValidation($model);

            $oldContextName = $model->context;

            $model->attributes=$_POST['Contexts'];

            try{
                if ($model->save()){
                    if ($oldContextName != $model->context){
                        $asteriskExts=new AsterixExtsFile();
                        $asteriskExts->updateSection($model->context,$oldContextName);
                    }

                    $result->set(0,'Контекст сохранён.',$model->id_context);
                }else{
                    throw new CHttpException(500,$model->getErrors());
                }
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            throw new CHttpException(500,'Контекст не найден.');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionRemoveContext($context){
        $model=Contexts::model()->findByPk($context);

        $result=new JsonResult();

        if($model){
            try{
                $contextName = $model->context;

                if ($model->delete()){
                    $asteriskExts=new AsterixExtsFile();
                    $asteriskExts->removeSection($contextName);

                    $result->set(0,'Контекст удален.',$model->id_context);
                }else{
                    throw new CHttpException(500,$model->getErrors());
                }
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            throw new CHttpException(500,'Контекст не найден.');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionNewContext(){
        $model=new Contexts();

        $result=new JsonResult();

        $this->performAjaxValidation($model);

        $model->attributes=$_POST['Contexts'];

        try{
            if ($model->save()){
                $asteriskExts=new AsterixExtsFile();
                $asteriskExts->addSection($model->context);

                $result->set(0,'Контекст сохранён.',$model->id_context);
            }else{
                throw new CHttpException(500,$model->getErrors());
            }
        }catch (Exception $e){
            throw new CHttpException(500,$e->getMessage());
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionNewExt(){
        $model=new Exts();

        $result=new JsonResult();

        $this->performAjaxValidation($model);

        $model->attributes=$_POST['Exts'];

        try{
            if ($model->save()){
                $result->set(0,'Расширение сохранено.',$model->id_ext);
            }else{
                throw new CHttpException(500,$model->getErrors());
            }
        }catch (Exception $e){
            throw new CHttpException(500,$e->getMessage());
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionRemoveExt($ext){
        $model=Exts::model()->findByPk($ext);

        $result=new JsonResult();

        if($model){
            try{
                if ($model->delete()){
                    $result->set(0,'Расширение удалено.',$model->id_ext);
                }else{
                    throw new CHttpException(500,$model->getErrors());
                }
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            throw new CHttpException(500,'Расширение не найдено.');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionEditExt($ext){
        $model=Exts::model()->findByPk($ext);

        $result=new JsonResult();

        if($model){
            $this->performAjaxValidation($model);
            $model->attributes=$_POST['Exts'];

            try{
                if ($model->save()){
                    $result->set(0,'Расширение сохранёно.',$model->id_ext);
                }else{
                    throw new CHttpException(500,$model->getErrors());
                }
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            throw new CHttpException(500,'Расширение не найдено.');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionNewApp(){
        $model=new Apps();

        $result=new JsonResult();

        $this->performAjaxValidation($model);

        $model->attributes=$_POST['Apps'];

        try{
            $appsNumber=Apps::model()->count('ext=:ext',array(':ext'=>$model->ext));
            $model->priority=(int)$appsNumber + 1;

            if ($model->save()){
                $result->set(0,'Команда сохранена.',$model->id_app);
                $result->data=$model;
            }else{
                throw new CHttpException(500,$model->getErrors());
            }
        }catch (Exception $e){
            throw new CHttpException(500,$e->getMessage());
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionEditApp($app){
        $model=Apps::model()->findByPk($app);

        $result=new JsonResult();

        if($model){
            $this->performAjaxValidation($model);
            $model->attributes=$_POST['Apps'];

            try{
                if ($model->save()){
                    $result->set(0,'Команда сохранёна.',$model->id_app);
                }else{
                    throw new CHttpException(500,$model->getErrors());
                }
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            $result->set(1,'',0,'Команда не найдена.');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionRemoveApp($app){
        $model=Apps::model()->findByPk($app);

        $result=new JsonResult();

        if($model){
            try{
                Apps::model()->updateAll(
                    array(
                        'priority'=>new CDbExpression('(priority-1)'),
                    ),
                    'priority>:priority AND ext=:ext',
                    array(
                        ':priority'=>$model->priority,
                        ':ext'=>$model->ext
                    )
                );

                if ($model->delete()){
                    $result->set(0,'Команда удалена.',$model->id_app);
                }else{
                    throw new CHttpException(500,$model->getErrors());
                }
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            throw new CHttpException(500,'Команда не найдена.');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionChangePriorityApp($id1,$id2){
        $app1=Apps::model()->findByPk($id1);
        $app2=Apps::model()->findByPk($id2);

        $result=new JsonResult();
        if (($app1)&&($app2)){
            try{
                $priority = $app1->priority;
                $app1->priority = $app2->priority;
                $app2->priority = $priority;

                $app1->update();
                $app2->update();

                $result->set(0,'Приоритете киманды изменен',$app1->id_app,'');
            }catch (Exception $e){
                throw new CHttpException(500,$e->getMessage());
            }
        }else{
            throw new CHttpException(500,'Не могу найти команды');
        }

        $this->render('json',array('json'=>$result->getJson()));
    }

    public function actionAsteriskApps(){
        $this->render('json',array('json'=>CJSON::encode($this->asteriskApps)));
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==true){
            header('Content-Type: application/json; charset=utf-8');
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
