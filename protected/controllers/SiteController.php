<?php

class SiteController extends Controller
{
    public $layout='//layouts/column1';
    private $asterisk='asterisk';
    private $initScript='/etc/init.d/asterisk';

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','login'),
                'users'=>array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','login','logout','getServerState','getAsteriskState','error','dialplanReload','IniFile','cdr'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actions()
	{
		return array(
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    private function isAstriskExists()
    {
        $output = exec('echo $PATH;2>&1',$op,$ret_val);
        $paths=explode(':',$output);

        foreach($paths as $path){
            if (file_exists($path.'/asterisk'))
                return true;
        }

        return false;
    }

    private function getAsteriskPath()
    {
        $output = exec('echo $PATH;2>&1',$op,$ret_val);
        $paths=explode(':',$output);

        foreach($paths as $path){
            if (file_exists($path.'/asterisk'))
                return $path.'/asterisk';
        }

        return null;
    }

    private function getAsteriskPid()
    {
        return exec('pidof asterisk');
    }

    private function initProcStatsArray()
    {
        return array('success'=>false);
    }

	public function actionIndex()
	{
        Yii::app()->session['stat_1']=$this->initProcStatsArray();
        Yii::app()->session['stat_2']=$this->initProcStatsArray();

		$this->render('index',
            array(
                'asteriskExists'=>$this->isAstriskExists()
            )
        );
	}

    public  function getProcsStat()
    {
        exec('cat /proc/stat;2>&1',$statData,$ret_val);

        $contProc=0;
        $stats=array();
        $stats['success']=true;
        $stats['cpus']=0;
        $stats['stats']=array();
        foreach ($statData as $line) {
            if (substr($line,0,3)=='cpu'){
                try{
                    $values=explode(' ',$line);

                    if (is_string((substr($values[0],3)))){
                        $stat=array();
                        $stat['user']=$values[1];
                        $stat['nice']=$values[2];
                        $stat['system']=$values[3];
                        $stat['idle']=$values[4];
                        $contProc++;
                        array_push($stats['stats'],$stat);
                    }
                }catch (Exception $e){
                    $stats['success']=false;
                }
            }
        }

        $stats['cpus']=$contProc;

        return $stats;
    }

    private function calcProcsLoadAvr()
    {
        $stats=$this->getProcsStat();

        $statCur= Yii::app()->session['stat_cur'];

        Yii::app()->session['stat_1']=Yii::app()->session['stat_2'];
        Yii::app()->session['stat_2']=$stats;

        $stat1=Yii::app()->session['stat_1'];
        $stat2=Yii::app()->session['stat_2'];

        $loadAvr=array();
        if (($stat1['success']==true)&& ($stat2['success']==true)){
            for ($i=0;$i<$stat1['cpus'];$i++){
                $s1=$stat1['stats'][$i];
                $s2=$stat2['stats'][$i];

                try {
                    $userd=$s2['user']-$s1['user'];
                    $niced=$s2['nice']-$s1['nice'];
                    $systemd=$s2['system']-$s1['system'];
                    $idled=$s2['idle']-$s1['idle'];

                    $devisor=$userd + $niced + $systemd + $idled;
                    if ($devisor!=0)
                        $load=100 * ($userd + $niced + $systemd)/$devisor;
                    else
                        $load=0;
                }catch (Exception $e){
                    $load=0;
                }

                array_push($loadAvr,round($load,2));
            }
        }

        return $loadAvr;
    }

    private function getProcsLoadAvr()
    {
        $loadAvr=$this->calcProcsLoadAvr();

        $count=count($loadAvr);

        if ($count==0)
            return null;

        $stats=array('cpus'=>$count,'loadavr'=>$loadAvr);

        return $stats;
    }

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogin()
	{
		$model=new LoginForm;

		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		$this->render('login',array('model'=>$model));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    private function getSystemMemInfo()
    {
        $data = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach ($data as $line) {
            if (trim($line)!=''){
                list($key, $val) = explode(":", $line);
                $meminfo[strtolower($key)] = str_replace('kB','',trim($val));
            }

        }
        return $meminfo;
    }

    private function getProcStatus($pid)
    {
        //$data = explode("\n", file_get_contents("/proc/26281/status"));
        exec('cat /proc/'.$pid.'/status;2>&1',$data,$ret_val);
        $status = array();
        foreach ($data as $line) {
            list($key, $val) = explode(":", $line);
            $status[strtolower($key)] = str_replace('kB','',trim($val));
        }
        return $status;
    }

    public function actionGetServerState()
    {
        header('Content-type: application/json');

        $meminfo=$this->getSystemMemInfo();

        try {
            $diskfree=round(disk_free_space('/')/(1024*1024*1024),2);
            $disktotal=round(disk_total_space('/')/(1024*1024*1024),2);
            $diskusage=$disktotal-$diskfree;
        }catch (Exception $e){
            $diskfree=0;
            $disktotal=0;
            $diskusage=0;
        }

        $serverState=array(
            'sizemem'=>'MB',
            'memtotal'=>floor($meminfo['memtotal']/1024),
            'memfree'=>floor($meminfo['memfree']/1024),
            'memusage'=>floor(($meminfo['memtotal']-$meminfo['memfree'])/1024),
            'sizeswap'=>'MB',
            'swaptotal'=>floor(($meminfo['swaptotal'])/1024),
            'swapfree'=>floor(($meminfo['swapfree'])/1024),
            'swapusage'=>floor(($meminfo['swaptotal']-$meminfo['swapfree'])/1024),
            'sizedisk'=>'GB',
            'disktotal'=>$disktotal,
            'diskfree'=>$diskfree,
            'diskusage'=>$diskusage,
            'cpuinfo'=>$this->getProcsLoadAvr()
        );

        echo CJSON::encode($serverState);
    }

    public function actionGetAsteriskState()
    {
        header('Content-type: application/json');

        $status=exec('/etc/init.d/asterisk status');
        $status=trim(str_replace('* status:','',$status));
        $pid=trim($this->getAsteriskPid());

        if (is_numeric($pid)){
            $procstatus=$this->getProcStatus($pid);
            $vmhwm=number_format($procstatus['vmhwm']/1024,2,'.',' ').' MB';
            $vmhwmraw=intval($procstatus['vmhwm']);
        }else{
            $vmhwm=0;
            $vmhwmraw=0;
            $pid=0;
        }

        echo CJSON::encode(array(
            'status' => $status,
            'pid' => $pid,
            'vmhwm' => $vmhwm,
            'vmhwmraw' => $vmhwmraw
        ));
    }

    public function actionDialplanReload($action)
    {
        $asteriskPath=$this->getAsteriskPath();
        if (!$asteriskPath)
            return;

        switch($action){
            case 'dr':
                $command='sudo '.$asteriskPath.' -x "dialplan reload"';
                break;
            case 'sr':
                $command='sudo '.$asteriskPath.' -x "sip reload"';
                break;
            case 'stop':
                $command='sudo '.$this->initScript.' stop;2>&1';
                break;
            case 'start':
                $command='sudo '.$this->initScript.' start > /dev/null 2>&1 &';
                break;
            case "restart":
                $command='sudo '.$this->initScript.' restart > /dev/null 2>&1 &';
                break;
            default:
                break;
        }

        $result=exec($command,$statData);
        for($i=0;$i<count($statData);$i++){
            echo $statData[$i].'<br>';
        }
    }

    public function actionIniFile()
    {
        $confFile=new ConfFile('chan_dahdi.conf');
        $confFile->addSection('dd');
    }

    public function actionCdr()
    {
        header('Content-Type: text/html; charset=utf-8');

        $dataProvider=new CActiveDataProvider('Cdr',array(
            'pagination'=>array(
                'pageSize'=>20
            )
        ));

        $this->setPageTitle('CDR');

        $this->render('cdr',array(
            'cdrs'=>$dataProvider
        ));
    }

}