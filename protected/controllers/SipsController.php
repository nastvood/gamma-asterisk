<?php

class SipsController extends Controller
{
	public $layout='//layouts/column1';

	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','AddSip','RemoveSip','updateSip','index','printShortSip'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionAddSip()
    {
        $this->layout='//layouts/column0';

        $model=new Sips;

        $this->performAjaxValidation($model);

        if(isset($_POST['Sips']))
        {
            $model->attributes=$_POST['Sips'];
            $model->regexten=$model->name;

            header('Content-type: application/json');
            try {
                if($model->save()){
                    Yii::app()->user->setFlash('success','SIP номер добавлен');
                    echo CJSON::encode('{"result":1}');
                }else{
                    echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
                }
            }catch (CDbException $e){
                echo CJSON::encode('{"result":0,"error":"Ошибка добавления записи"}');
            }
            Yii::app()->end();
        }

        $contexts=Extensions::model()->getContexts();

        $this->render('addSip',array(
            'model'=>$model,
            'contexts'=>$contexts,
            'types'=>Sips::model()->types(),
            'transports'=>Sips::model()->transports(),
        ));
    }

    public function actionRemoveSip($id)
    {
        $this->layout='//layouts/column0';

        header('Content-type: application/json');

        $sip=Sips::model()->findByPk($id);
        if ($sip===null){
            Yii::app()->user->setFlash('danger','Ошибка удаления SIP номера');
            echo CJSON::encode('{"result":0,"error":"Запись не найдена"}');
            Yii::app()->end();
        }

        $sip->delete();

        if ($sip->hasErrors()){
            Yii::app()->user->setFlash('danger','Ошибка удаления SIP номера ('.$this->getFirstErrorModel($sip).')');
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($sip).'"}');
        }else{
            Yii::app()->user->setFlash('success','SIP номер удален');
            echo CJSON::encode('{"result":1}');
        }

        Yii::app()->end();
    }

    public function actionUpdateSip($id)
    {
        $this->layout='//layouts/column0';

        $model=$this->loadModel($id);
        if ($model===null){
            header('Content-type: application/json');
            echo CJSON::encode('{"result":0,"error":"Запись не найдена"}');
            Yii::app()->end();
        }

        $this->performAjaxValidation($model);

        if(isset($_POST['Sips']))
        {
            $model->attributes=$_POST['Sips'];
            $model->regexten=$model->name;

            header('Content-type: application/json');
            try {
                if($model->save()){
                    Yii::app()->user->setFlash('success','SIP номер изменен');
                    echo CJSON::encode('{"result":1}');
                }else{
                    echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
                }
            }catch (CDbException $e){
                echo CJSON::encode('{"result":0,"error":"Ошибка редактирования записи"}');
            }
            Yii::app()->end();
        }

        $contexts=Extensions::model()->getContexts();

        $this->render('updateSip',array(
            'model'=>$model,
            'contexts'=>$contexts,
            'types'=>Sips::model()->types(),
            'transports'=>Sips::model()->transports(),
            'id'=>$id
        ));
    }

	public function actionIndex()
	{
        $this->setPageTitle("SIP");
        $sips=Sips::model()->findAll(array('order'=>'name'));
		$this->render('index',array(
			'sips'=>$sips
		));
	}

	public function loadModel($id)
	{
		$model=Sips::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sips-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionPrintShortSip()
    {
        $this->setPageTitle("Печать SIP");
        $this->layout='//layouts/print';

        $sips=Sips::model()->findAll(array('order'=>'name'));

        $this->render('printShortSip',
            array(
                'sips'=>$sips
            )
        );
    }
}
