<?php
class ExtsController extends Controller
{
    public $layout='//layouts/column1';

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('modals','index'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex(){
        $this->setPageTitle("Расширения");

        $this->render('index');
    }

    public function actionModals(){
        $this->renderPartial('modals');
    }
}