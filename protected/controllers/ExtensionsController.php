<?php

class ExtensionsController extends Controller
{
	public $layout='//layouts/column1';

    public $asteriskApps=array('Answer','Background','Dial','Goto','Gotoif','Hangup','NoOp','Playback','Set','SetMusicOnHold','VoiceMail','VoiceMailMain','WaitExten');

	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','extensions','index',
                                'apps','updateApp','changePriority',
                                'addApp','removeApp','addExten',
                                'getExtenNameById','updateExten','removeExten',
                                'addContext','removeContext','updateContext'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionGetExtenNameById($id){
        header('Content-type: application/json');
        $extension=Extensions::model()->findByPk($id);
        if($extension)
            echo CJSON::encode('{"result":1,"name":"'.$extension->exten.'"}');
        else
            echo CJSON::encode('{"result":0}');
    }

    public function actionAddExten($exten,$name=null)
    {
        $this->layout='//layouts/column0';

        $curContext=Extensions::model()->findByPk($exten);

        $model=new Extensions;
        $model->context=$curContext->context;
        $model->comment=$curContext->comment;
        $model->exten=$name;
        $model->priority=1;
        $model->app="Answer";

        header('Content-type: application/json');
        if($model->save()){
            Yii::app()->user->setFlash('success','Расширение добавлено');
            echo CJSON::encode('{"result":1,"exten":'.$model->id.'}');
        }else
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');

        Yii::app()->end();
    }

    public function actionAddContext($context=null,$exten=null,$comment=null)
    {
        $this->layout='//layouts/column0';

        $model=new Extensions;
        $model->context=$context;
        $model->exten=$exten;
        $model->comment=$comment;
        $model->priority=1;
        $model->app="Answer";

        header('Content-type: application/json');
        if (!($model->validate('exten') && $model->validate('context'))){
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
            Yii::app()->end();
        }

        try{
            if($model->save()){
                $asteriskExts=new AsterixExtsFile();
                $asteriskExts->addSection($model->context);

                Yii::app()->user->setFlash('success','Контекст добавлен');
                echo CJSON::encode('{"result":1,"exten":'.$model->id.'}');
            }else
                echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
        }catch (Exception $e){
            echo CJSON::encode('{"result":0,"error":"'.$e->getMessage().'"}');
        }

        Yii::app()->end();
    }

    public function actionAddApp($exten=null)
    {
        $this->layout='//layouts/column0';

        $model=new Extensions;

        $this->performAjaxValidation($model);

        if(isset($_POST['Extensions']))
        {
            $model->attributes=$_POST['Extensions'];

            header('Content-type: application/json');

            if ($exten){
                $app=Extensions::model()->findByPk($exten);
                if ($app){
                    $model->context=$app->context;
                    $model->exten=$app->exten;
                    $model->comment=$app->comment;
                }
            }

            if($model->save()){
                Yii::app()->user->setFlash('success','Команда добавлена');
                echo CJSON::encode('{"result":1,"exten":'.$exten.'}');
            }else
                echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');

            Yii::app()->end();
        }else{
            if ($exten){
                $app=Extensions::model()->findByPk($exten);
                if ($app){
                    $model->context=$app->context;
                    $model->exten=$app->exten;
                    $model->comment=$app->comment;

                    $maxPriority=Extensions::model()->find(
                        array(
                            'condition'=>'context=:context AND exten=:exten',
                            'params'=>array(':context'=>$model->context,':exten'=>$model->exten),
                            'order'=>'priority DESC'
                        )
                    );

                    if(!$maxPriority)
                        $model->priority=1;
                    else{
                        $model->priority=$maxPriority->priority+1;
                    }
                }
            }
        }

        $this->render('addApp',array(
            'model'=>$model,
            'exten'=>$exten,
            'asteriskApps'=>$this->asteriskApps
        ));
    }

    public function actionRemoveApp($id)
    {
        $this->layout='//layouts/column0';

        $extension=Extensions::model()->findByPk($id);
        $context=$extension->context;
        $exten=$extension->exten;

        $extension->delete();

        $cur_context=Extensions::model()->find(
            array(
                'condition'=>'context=:context AND exten=:exten',
                'order'=>'priority',
                'params'=>array(
                    ':context'=>$context,
                    ':exten'=>$exten
                )
            )
        );

        if (!$cur_context){
            $cur_context=Extensions::model()->find(
                array(
                    'condition'=>'context=:context',
                    'order'=>'context,exten,priority',
                    'params'=>array(
                        ':context'=>$context,
                    )
                )
            );
        }

        header('Content-type: application/json');
        if ($extension->hasErrors())
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($extension).'"}');
        else
            echo CJSON::encode('{"result":1,"context":'.($cur_context?$cur_context->id:0).'}');

        Yii::app()->end();
    }

    public function actionRemoveExten($exten)
    {
        $this->layout='//layouts/column0';

        $extension=Extensions::model()->findByPk($exten);
        $extension->deleteAll(
            'context=:context AND exten=:exten',
            array(
                ':context'=>$extension->context,
                ':exten'=>$extension->exten
            )
        );

        header('Content-type: application/json');
        if ($extension->hasErrors())
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($extension).'"}');
        else{
            Yii::app()->user->setFlash('success','Расширение удалено');
            echo CJSON::encode('{"result":1}');
        }

        Yii::app()->end();
    }

    public function actionRemoveContext($exten)
    {
        $this->layout='//layouts/column0';

        header('Content-type: application/json');
        try{
            $extension=Extensions::model()->findByPk($exten);
            $context=$extension->context;
            $extension->deleteAll(
                'context=:context',
                array(
                    ':context'=>$context,
                )
            );

            if ($extension->hasErrors()){
                Yii::app()->user->setFlash('success','Ошибка удаления контекста');
                echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($extension).'"}');
            }else{
                $asteriskExts=new AsterixExtsFile();
                $asteriskExts->removeSection($context);

                Yii::app()->user->setFlash('success','Контекст удален');
                echo CJSON::encode('{"result":1}');
            }
        }catch (Exception $e){
            Yii::app()->user->setFlash('success','Ошибка удаления контекста');
            echo CJSON::encode('{"result":0,"error":"'.$e->getMessage().'"}');
        }

        Yii::app()->end();
    }

	public function actionIndex($id=null)
	{
        $this->setPageTitle("Расширения");

        if (Yii::app()->user->isGuest){
            $this->redirect("/site/login");
            return;
        }

        $contexts_models=Yii::app()->db->createCommand()
                        ->select('context,exten,id')
                        ->from('extensions')
                        ->group('context,exten,priority')
                        ->having('priority=1')
                        ->order('context,exten,priority')
                        ->queryAll();

        //print_r($contexts_models);

        $exten=0;
        $context='';

        if ($id){
            $context_exten=Extensions::model()->findByPk($id);
            if ($context_exten){
                $cur_context=Extensions::model()->find(
                    array(
                        'condition'=>'context=:context AND exten=:exten',
                        'order'=>'priority',
                        'params'=>array(
                            ':context'=>$context_exten->context,
                            ':exten'=>$context_exten->exten
                        )
                    )
                );

                $context=$cur_context->context;
                $exten=$cur_context->id;
            }else
                $id=null;
        }

        if ($contexts_models){
            if (!$id){
                $context=$contexts_models[0]['context'];
                $exten=$contexts_models[0]['id'];
            }

            $contexts=array();
            $oldContext='';
            foreach ($contexts_models as $c){
                $newContext=$c['context'];
                if ($oldContext!=$newContext){
                    $contexts[$newContext]=array();
                    $oldContext=$newContext;
                }

                $contexts[$newContext][$c['id']]=$c['exten'];
            }
        }

		$this->render('index',array(
            'contexts'=>$contexts,
            'curContext'=>$context,
            'curExten'=>$exten,
		));
	}

    public function actionExtensions($context)
    {
        $this->layout='//layouts/column0';

        $extensions=Extensions::model()->findAll(
            array(
                'distinct'=>true,
                'condition'=>'context=:context',
                'group'=>'exten',
                'params'=>array(':context'=>$context)
            )
        );

        $this->render('extensions',array(
                'extensions'=>$extensions,
                'contextId'=>md5($context),
                'context'=>$context
            )
        );
    }

    public function actionApps($id)
    {
        $this->layout='//layouts/column0';

        $exten=Extensions::model()->findByPk($id);
        $apps=null;

        if ($exten){
            $apps=Extensions::model()->findAll(
                array(
                    'condition'=>'context=:context AND exten=:exten',
                    'params'=>array(':context'=>$exten->context,':exten'=>$exten->exten),
                    'order'=>'priority'
                )
            );
        }

        $this->render('apps',array(
                'exten'=>$exten,
                'apps'=>$apps
            )
        );
    }

    public function actionChangePriority($exten,$id,$up){
        $app=Extensions::model()->findByPk($id);

        if ($id==0)
            Yii::app()->end();

        if ($app){
            $appUp=null;
            if ($up>0)
                $appUp=Extensions::model()->find(
                    array(
                        'condition'=>'context=:context AND exten=:exten AND priority>:priority',
                        'order'=>'priority',
                        'params'=>array(':context'=>$app->context,':exten'=>$app->exten,':priority'=>$app->priority)
                    )
                );
            else
                $appUp=Extensions::model()->find(
                    array(
                        'condition'=>'context=:context AND exten=:exten AND priority<:priority',
                        'order'=>'priority DESC',
                        'params'=>array(':context'=>$app->context,':exten'=>$app->exten,':priority'=>$app->priority)
                    )
                );

            if($appUp){
                $priority=$app->priority;
                $app->priority=$appUp->priority;
                $appUp->priority=$priority;

                if (($appUp->save()) && ($app->save()))
                    $this->redirect('/extensions/index/id/'.$exten);
            }
        }
    }

    public function actionUpdateApp($exten,$id)
    {
        $this->layout='//layouts/column0';

        $model=$this->loadModel($id);

        $this->performAjaxValidation($model);

        if(isset($_POST['Extensions']))
        {
            $model->attributes=$_POST['Extensions'];

            header('Content-type: application/json');
            if($model->save()){
                Yii::app()->user->setFlash('success','Команда обновлена');
                echo CJSON::encode('{"result":1}');
            }
            else
                echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');

            Yii::app()->end();
        }

        $this->render('updateApp',array(
            'model'=>$model,
            'exten'=>$exten,
            'asteriskApps'=>$this->asteriskApps
        ));
    }

    public function actionUpdateExten($exten,$name=null)
    {
        $this->layout='//layouts/column0';

        header('Content-type: application/json');

        $model=$this->loadModel($exten);
        if (!$model){
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
            Yii::app()->end();
        }

        $oldName=$model->exten;
        $model->exten=$name;
        if (!$model->validate(array('exten'))){
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
            Yii::app()->end();
        }

        $rowsNumber=$model->updateAll(
            array(
                'exten'=>$name
            ),
            'context=:context AND exten=:exten',
            array(
                ':context'=>$model->context,
                ':exten'=>$oldName,
            )
        );

        if($rowsNumber>0){
            Yii::app()->user->setFlash('success','Расширение обновлено');
            echo CJSON::encode('{"result":1,"exten":'.$exten.'}');
        }else{
            $err=$this->getFirstErrorModel($model)==''?'Обновлять нечего':$this->getFirstErrorModel($model);
            echo CJSON::encode('{"result":0,"error":"'.$err.'"}');
        }

        Yii::app()->end();
    }

    public function actionUpdateContext($exten,$context=null,$comment=null)
    {
        $this->layout='//layouts/column0';

        header('Content-type: application/json');

        $model=$this->loadModel($exten);
        $oldContext=$model->context;
        $model->context=$context;
        $model->comment=$comment;
        if (!$model->validate(array('context'))){
            echo CJSON::encode('{"result":0,"error":"'.$this->getFirstErrorModel($model).'"}');
            Yii::app()->end();
        }

        try{
            $rowsNumber=$model->updateAll(
                array(
                    'context'=>$context,
                    'comment'=>$comment
                ),
                'context=:context',
                array(
                    ':context'=>$oldContext
                )
            );

            if($rowsNumber>0){
                $asteriskExts=new AsterixExtsFile();
                $asteriskExts->updateSection($context,$oldContext);

                Yii::app()->user->setFlash('success','Контекст обновлен');
                echo CJSON::encode('{"result":1,"exten":'.$exten.'}');
            }
            else
                echo CJSON::encode('{"result":0,"error":"Записывать нечего!"}');
        }catch (Exception $e){
            echo CJSON::encode('{"result":0,"error":"'.$e->getMessage().'"}');
        }

        Yii::app()->end();
    }

	public function loadModel($id)
	{
		$model=Extensions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='extensions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
