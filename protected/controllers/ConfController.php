<?php

class ConfController extends Controller
{
    public $layout='//layouts/column1';

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','addsetting','updatesetting'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    public function actionIndex($c)
	{
        $confFile=AsteriskConfigs::createConfigParser($c);
        $confs=$confFile->parseFileContent();

        $content=array();
        foreach($confs as $key=>$value){
            if (count($value)==0){
                $confLine=array('section'=>$key,'key'=>'','value'=>'');
                array_push($content,$confLine);
            }else{
                for($i=0;$i<count($value);$i++){
                    $equal_sign=EqualSignConf::getSign($value[$i]['equal_sign']);
                    foreach ($value[$i] as $keyLine=>$valueLine) {
                        if ($keyLine!='equal_sign'){
                            $confLine=array('section'=>$key,'key'=>$keyLine,'value'=>$valueLine,'pos'=>$i,'equal_sign'=>$equal_sign);
                            array_push($content,$confLine);
                        }
                    }
                }
            }
        }

        //print_r($content);

        $this->setPageTitle($confFile->getFileName());

		$this->render('index',
            array(
                'content'=>$content,
                'conf'=>$c,
                'fileName'=>$confFile->getFileName()
            )
        );
	}

    public function actionAddSetting($section,$key,$equal,$value,$pos)
    {
        /*$dahdiFile=new AsteriskChanDahdiFile();
        $dahdiFile->addSetting($section,$key,$equal,$value,$pos);*/
    }

    public function  actionUpdateSetting($section,$key,$equal,$value,$pos)
    {
        /*$dahdiFile=new AsteriskChanDahdiFile();
        $dahdiFile->updateSetting($section,$key,$equal,$value,$pos);*/
    }


}