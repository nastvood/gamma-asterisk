<?php
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Gamma-asterisk',
    'language'=>'ru',
    'sourceLanguage'=>'ru',

	//'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'4ster1sk',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1','10.0.2.10'),
		),
	),

	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>true,
		),

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false
		),


		'db'=>array(
			'connectionString' => 'mysql:dbname=asterisk;host=127.0.0.1;',
			'emulatePrepare' => true,
			'username' => 'asterisk',
			'password' => '4ster1sk',
			'charset' => 'utf8'
		),

		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),

		/*'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
		/*	),
		),*/
	),

	'params'=>array(
		'adminEmail'=>'nastvood@gmail.com',
	),
);