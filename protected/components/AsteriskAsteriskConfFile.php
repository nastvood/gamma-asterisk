<?php
/**
 * Created by JetBrains PhpStorm.
 * User: agapiy
 * Date: 30.08.13
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */

class AsteriskAsteriskConfFile extends ConfFile{

    public function __construct()
    {
        parent::__construct('/etc/asterisk/asterisk.conf');
    }
}