<?php

class EqualSignConf{
    const __default=self::EQUAL;

    const EQUAL=0;
    const ARROW=1;
    const NONE=2;

    public static function getSign($const)
    {
        switch ($const){
            case self::EQUAL:
                return "=";
            case self::ARROW:
                return "=>";
            case self::NONE:
                return "";
            default:
                return "=";
        }
    }

    public static function getList()
    {
        return array(
            self::EQUAL=>'=',
            self::ARROW=>'=>',
            self::NONE=>''
        );
    }
}

class ConfFile
{
    protected $user;
    protected $group;
    protected $assetPath;
    protected $confFile;
    protected $fileOwner;
    protected $fileGroup;
    protected $dirFile;
    protected $filePath;

    public function getFileName()
    {
        return $this->confFile;
    }

    protected function getFileOwner($filePath)
    {
        return exec('sudo stat -c "%U" "'.$filePath.'";2>&1');
    }

    protected function getFileGroup($filePath)
    {
        return exec('sudo stat -c "%U" "'.$filePath.'";2>&1');
    }

    protected function init($filePath){
        $pathinfo=pathinfo($filePath);

        $this->confFile = $pathinfo['basename'];
        $this->dirFile=$pathinfo['dirname'];
        $this->filePath=$filePath;

        $this->user=get_current_user();
        $groupInfo = posix_getgrgid(posix_getgid());
        $this->group =  $groupInfo['name'];

        $this->assetPath=Yii::app()->assetManager->basePath;

        $this->fileOwner=$this->getFileOwner($filePath);
        $this->fileGroup=$this->getFileGroup($filePath);
    }

    public function __construct($filePath)
    {
        $this->init($filePath);
    }

    protected function updateTmpFile()
    {
        $command='rm -rf "'.$this->assetPath.'/'.$this->confFile.'"';
        exec($command);
        $command='sudo cp -f "'.$this->filePath.'" "'.$this->assetPath.'"';
        exec($command);
        $command='sudo chown '.$this->user.':'.$this->group.' "'.$this->assetPath.'/'.$this->confFile.'"';
        exec($command);

        $fp=fopen($this->assetPath.'/'.$this->confFile,'r');
        if (!$fp)
            return false;

        fclose($fp);

        return true;
    }

    protected  function updateConfFile()
    {
        $command='sudo cp -f "'.$this->assetPath.'/'.$this->confFile.'" "'.$this->filePath.'"';
        exec($command);
        $command='sudo chown '.$this->fileOwner.':'.$this->fileGroup.' "'.$this->filePath.'"';
        exec($command);
    }

    protected function save($content)
    {
        $fp=fopen($this->assetPath.'/'.$this->confFile,'w');
        if (!$fp)
            throw new Exception('Не могу открыть файл расширений');

        fwrite($fp,$content);

        fclose($fp);

        $this->updateConfFile();
    }

    public function parseFileContent()
    {
        if (!$this->updateTmpFile())
            throw new Exception('Не могу открыть файл расширений');

        $lines=file($this->assetPath.'/'.$this->confFile);
        $confs=array();
        $curContext='';
        foreach ($lines as $line_num => $line){
            $line=trim($line);

            if ($line[0]==';')
                continue;

            if ($line=='')
                continue;

            if ($line[0]=='['){
                $pos=strpos($line,']');
                if ($pos>1){
                    $curContext=substr($line,1,$pos-1);
                    $confs[$curContext]=array();
                }
            }else{
                if ($curContext!='')
                    $posEqual=strpos($line,'=');
                    if ($posEqual<1)
                        continue;

                    $keyValue=array(substr($line,0,$posEqual),substr($line,$posEqual+1));
                if (count($keyValue)==2){
                    $value=$keyValue[1];
                    $equal_sign=EqualSignConf::EQUAL;
                    if (substr($value,0,1)==">"){
                        $value=substr($value,1);
                        $equal_sign=EqualSignConf::ARROW;
                    }

                    array_push($confs[$curContext],array(trim($keyValue[0])=>trim($value),'equal_sign'=>$equal_sign));
                }
            }
        }

        return $confs;
    }

    private function arrayToFileContent($confs)
    {
        $content='';
        foreach($confs as $section=>$settings){
            $content.='['.$section.']'.PHP_EOL;
            foreach($settings as $pos=>$setting){
                if (count($setting)==2){
                    $equalSign=EqualSignConf::getSign($setting['equal_sign']);
                    foreach($setting as $key=>$value){
                        if ($key!='equal_sign')
                            $content.=$key.$equalSign.$value.PHP_EOL;
                    }
                }
            }
        }

        return $content;
    }

    public function addSetting($section,$key,$equal,$value,$pos=0)
    {
        $confs=$this->parseFileContent();

        $newSetting=array(
            $key=>$value,
            'equal_sign'=>$equal
        );

        if (!array_key_exists($section,$confs))
            throw new Exception('Не могу найти секцию');

        array_splice($confs[$section],$pos,0,array($newSetting));

        $content=$this->arrayToFileContent($confs);
        echo $content;

        //$this->save($content);
    }

    public function updateSetting($section,$key,$equal,$value,$pos)
    {
        $confs=$this->parseFileContent();

        $newSetting=array(
            $key=>$value,
            'equal_sign'=>$equal
        );

        if (!array_key_exists($section,$confs))
            throw new Exception('Не могу найти секцию');

        $confs[$section][$pos]=$newSetting;

        $content=$this->arrayToFileContent($confs);
        echo $content;

        //$this->save($content);
    }

    public function addSection($section)
    {
        if (!$this->updateTmpFile())
            throw new Exception('Не могу открыть файл расширений');

        //$section=$this->parseFileContent();
    }
}