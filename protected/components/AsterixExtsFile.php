<?php
/**
 * Created by JetBrains PhpStorm.
 * User: agapiy
 * Date: 07.08.13
 * Time: 13:15
 * To change this template use File | Settings | File Templates.
 */

class AsterixExtsFile extends ConfFile {
    public function __construct()
    {
        parent::__construct('/etc/asterisk/extensions.conf');
    }

    public function parseFileContent()
    {
        if (!$this->updateTmpFile())
            throw new Exception('Не могу открыть файл расширений');

        $lines=file($this->assetPath.'/extensions.conf');
        $exts=array();
        $curContext='';
        foreach ($lines as $line_num => $line) {
            $line=trim($line);

            if ($line=='')
                continue;

            if ($line[0]=='['){
                if ($line[strlen($line)-1]==']'){
                    $curContext=substr($line,1,strlen($line)-2);
                    $exts[$curContext]=array();
                }
            }else{
                if ($curContext!='')
                    array_push($exts[$curContext],$line);
            }
        }

        return $exts;
    }

    public function addSection($ext)
    {
        $exts=$this->parseFileContent();

        $exts[$ext]=array('switch => Realtime/@');

        $this->save($exts);
    }

    public function removeSection($ext)
    {
        $exts=$this->parseFileContent();

        unset($exts[$ext]);

        $this->save($exts);
    }

    public function updateSection($extNew,$extOld)
    {
        $exts=$this->parseFileContent();

        unset($exts[$extOld]);
        $exts[$extNew]=array('switch => Realtime/@');

        $this->save($exts);
    }

    protected function save($exts)
    {
        $fp=fopen($this->assetPath.'/'.$this->confFile,'w');
        if (!$fp)
            throw new Exception('Не могу открыть файл расширений');

        $content='';
        foreach($exts as $key=>$value){
            $content.='['.$key.']'.PHP_EOL;
            if (is_array($value)){
                for($i=0;$i<count($value);$i++)
                    $content.=$value[$i].PHP_EOL;
            }
            $content.=PHP_EOL;
        }

        fwrite($fp,$content);

        fclose($fp);

        $this->updateConfFile();
    }
}