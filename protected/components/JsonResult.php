<?php

class JsonResult
{
    public $err=0;
    public $errMsg=null;
    public $msg='';
    public $id=0;
    public $data=null;

    public function __construct($err=0,$msg='',$id=0,$errMsg=null){
        $this->err=$err;
        $this->msg=$msg;
        $this->id=$id;
        $this->errMsg=$errMsg;
    }

    public function set($err,$msg,$id=0,$errMsg=null){
        $this->err=$err;
        $this->msg=$msg;
        $this->id=$id;
        $this->errMsg=$errMsg;
    }

    public function getJson(){
        return CJSON::encode($this);
    }

    public function json($err,$msg,$id=0,$errMsg=null){
        $this->err=$err;
        $this->msg=$msg;
        $this->id=$id;
        $this->errMsg=$errMsg;

        return CJSON::encode($this);
    }
}