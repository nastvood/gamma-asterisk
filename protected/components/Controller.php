<?php

class Controller extends CController
{

	public $layout='//layouts/column1';

	public $menu=array();

	public $breadcrumbs=array();

    public function getFirstErrorModel($model)
    {
        if ($model->hasErrors()){
            $errors=array_values($model->getErrors());
            if (is_array($errors[0]))
                return $errors[0][0];
            else
                return $errors[0];
        }else
            return '';
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
}