<?php
/**
 * Created by JetBrains PhpStorm.
 * User: agapiy
 * Date: 30.08.13
 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */

class AsteriskSipConf extends ConfFile{

    public function __construct()
    {
        parent::__construct('/etc/asterisk/sip.conf');
    }
}