<?php
/**
 * Created by JetBrains PhpStorm.
 * User: agapiy
 * Date: 30.08.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */

class AsteriskConfigs
{
    const CHAN_DAHDY=0;
    const ASTERISK=1;
    const FEATURES=2;
    const EXTCONFIG=3;
    const SIP=4;
    const MODULES=5;

    static public function createConfigParser($const)
    {
        switch ($const){
            case self::CHAN_DAHDY:
                return new AsteriskChanDahdiFile();
            case self::ASTERISK:
                return new AsteriskAsteriskConfFile();
            case self::FEATURES:
                return new AsteriskFeaturesConfFile();
            case self::EXTCONFIG:
                return new AsteriskExtconfigConfFile();
            case self::SIP:
                return new AsteriskSipConf();
            case self::MODULES:
                return new AsteriskModulesConf();
            default:
                return new AsteriskChanDahdiFile();
        }
    }
}