<?php

class Cdr extends CActiveRecord
{
    public function defaultScope()
    {
        return array(
            'order'=>'calldate DESC'
        );
    }

	public function tableName()
	{
		return 'cdr';
	}

	public function rules()
	{
		return array(
			array('duration, billsec, amaflags, sequence', 'numerical', 'integerOnly'=>true),
			array('clid, src, dst, dcontext, channel, dstchannel, lastapp, lastdata', 'length', 'max'=>80),
			array('disposition', 'length', 'max'=>45),
			array('accountcode, peeraccount', 'length', 'max'=>20),
			array('uniqueid, linkedid', 'length', 'max'=>32),
			array('userfield', 'length', 'max'=>255),
			array('calldate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('calldate, clid, src, dst, dcontext, channel, dstchannel, lastapp, lastdata, duration, billsec, disposition, amaflags, accountcode, uniqueid, userfield, peeraccount, linkedid, sequence', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'calldate' => 'Дата/Время',
			'clid' => 'Clid',
			'src' => 'Ист',
			'dst' => 'Прием',
			'dcontext' => 'Прим. конт.',
			'channel' => 'Канал',
			'dstchannel' => 'Источник',
			'lastapp' => 'Прил',
			'lastdata' => 'Данные',
			'duration' => 'Прод.',
			'billsec' => 'Billsec',
			'disposition' => 'Disposition',
			'amaflags' => 'Amaflags',
			'accountcode' => 'Accountcode',
			'uniqueid' => 'Uniqueid',
			'userfield' => 'Userfield',
			'peeraccount' => 'Peeraccount',
			'linkedid' => 'Linkedid',
			'sequence' => 'Sequence',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('calldate',$this->calldate,true);
		$criteria->compare('clid',$this->clid,true);
		$criteria->compare('src',$this->src,true);
		$criteria->compare('dst',$this->dst,true);
		$criteria->compare('dcontext',$this->dcontext,true);
		$criteria->compare('channel',$this->channel,true);
		$criteria->compare('dstchannel',$this->dstchannel,true);
		$criteria->compare('lastapp',$this->lastapp,true);
		$criteria->compare('lastdata',$this->lastdata,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('billsec',$this->billsec);
		$criteria->compare('disposition',$this->disposition,true);
		$criteria->compare('amaflags',$this->amaflags);
		$criteria->compare('accountcode',$this->accountcode,true);
		$criteria->compare('uniqueid',$this->uniqueid,true);
		$criteria->compare('userfield',$this->userfield,true);
		$criteria->compare('peeraccount',$this->peeraccount,true);
		$criteria->compare('linkedid',$this->linkedid,true);
		$criteria->compare('sequence',$this->sequence);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
