<?php

class Sips extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'sipfriends';
    }

    public static function transports(){
        return array(
            'udp',
            'tcp',
            'udp,tcp',
            'tcp,udp'
        );
    }

    public static function types(){
        return array(
            'friend',
            'user',
            'peer'
        );
    }

    public function rules()
    {
        return array(
            array('name,host,type,context,secret,transport,language,fullname','required'),
            array('name', 'length', 'max'=>10),
            array('name','identity'),
            array('host,context,secret,language,mailbox,regexten,fullname', 'length', 'max'=>40),
            array('transport','in','range'=>$this->transports()),
            array('type','in','range'=>$this->types()),
            array('id,name,host,type,context,secret,transport,language,mailbox,regexten,fullname', 'safe', 'on'=>'search')
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'SIP',
            'host' => 'Хост',
            'type' => 'Тип',
            'context' => 'Контекст',
            'secret' => 'Пароль',
            'transport' => 'Транспорт',
            'language' => 'Локаль',
            'mailbox' => 'Mailbox',
            'regexten' => 'Команда',
            'fullname' => 'Имя'
        );
    }

    public function identity($attribute,$params)
    {
        try{
            if ($this->id===null)
                $rows=$this->findAll(
                    array(
                        'condition'=>$attribute.'=:attribute',
                        'params'=>array(':attribute'=>$this->$attribute)
                    )
                );
            else
                $rows=$this->findAll(
                    array(
                        'condition'=>'id<>:id AND '.$attribute.'=:attribute',
                        'params'=>array(
                            ':id'=>$this->id,
                            ':attribute'=>$this->$attribute
                        )
                    )
                );
        }catch (Exception $e){
            $this->addError($attribute, $e->getMessage());
        }

        if ($rows){
            $labels=$this->attributeLabels();
            $this->addError($attribute, 'Нельзя повторять значение поля - '.$labels[$attribute].'!');
        }
    }

    public function search()
    {
        $criteria=new CDbCriteria;

        /*$criteria->compare('id',$this->id,true);
        $criteria->compare('context',$this->context,true);
        $criteria->compare('exten',$this->exten,true);
        $criteria->compare('priority',$this->priority);
        $criteria->compare('app',$this->app,true);
        $criteria->compare('appdata',$this->appdata,true);*/

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}