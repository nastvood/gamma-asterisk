<?php

class Extensions extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'extensions';
	}

	public function rules()
	{
		return array(
			array('priority', 'numerical', 'integerOnly'=>true),
            array('exten,context,app','required'),
			array('context, exten, app', 'length', 'max'=>20),
            array('comment', 'length', 'max'=>256),
			array('appdata', 'length', 'max'=>128),
			array('id, context, exten, priority, app, appdata,comment', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'context' => 'Контекст',
			'exten' => 'Расширение',
			'priority' => 'Приоритет',
			'app' => 'Команда',
			'appdata' => 'Параметры',
            'comment' => 'Комментарий',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('context',$this->context,true);
		$criteria->compare('exten',$this->exten,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('app',$this->app,true);
		$criteria->compare('appdata',$this->appdata,true);
        $criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getContexts()
    {
        $contexts_model=Extensions::model()->findAll(
            array(
                'distinct'=>true,
                'select'=>'context',
                'order'=>'context'
            )
        );

        $contexts=array();
        foreach($contexts_model as $context){
            array_push($contexts,$context->context);
        }

        return $contexts;
    }

    public function delete(){
        Extensions::model()->updateAll(
            array(
                'priority'=>new CDbExpression('(priority-1)'),
            ),
            'priority>:priority AND context=:context AND exten=:exten',
            array(
                ':priority'=>$this->priority,
                ':context'=>$this->context,
                ':exten'=>$this->exten
            )
        );

        parent::delete();
    }
}