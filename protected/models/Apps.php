<?php

class Apps extends CActiveRecord
{
	public function tableName()
	{
		return 'Apps';
	}

	public function rules()
	{
		return array(
			array('ext, app', 'required'),
			array('priority', 'numerical', 'integerOnly'=>true),
			array('ext', 'length', 'max'=>10),
			array('app', 'length', 'max'=>20),
			array('appdata', 'length', 'max'=>128),
			array('id_app, ext, priority, app, appdata', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'extensions' => array(self::BELONGS_TO, 'Exts', 'ext'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_app' => 'Id App',
			'ext' => 'Ext',
			'priority' => 'Priority',
			'app' => 'App',
			'appdata' => 'Appdata',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_app',$this->id_app,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('app',$this->app,true);
		$criteria->compare('appdata',$this->appdata,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
