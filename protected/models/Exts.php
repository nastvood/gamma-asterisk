<?php

class Exts extends CActiveRecord
{
	public function tableName()
	{
		return 'Exts';
	}

    public function rules()
	{
		return array(
			array('context,ext', 'required'),
			array('context', 'length', 'max'=>10),
			array('ext', 'length', 'max'=>20),
			array('id_ext, context, ext', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'apps' => array(self::HAS_MANY, 'Apps', 'ext'),
			'contexts' => array(self::BELONGS_TO, 'Contexts', 'context'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_ext' => 'Id Ext',
			'context' => 'Context',
			'ext' => 'Ext',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_ext',$this->id_ext,true);
		$criteria->compare('context',$this->context,true);
		$criteria->compare('ext',$this->ext,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
