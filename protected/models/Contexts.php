<?php

class Contexts extends CActiveRecord
{
	public function tableName()
	{
		return 'Contexts';
	}

	public function rules()
	{
		return array(
			array('comment', 'length', 'max'=>1024),
			array('context', 'length', 'max'=>20),
            array('context,comment','required'),
			array('id_context, comment, context', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'exts' => array(self::HAS_MANY, 'Exts', 'context'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_context' => 'Id Context',
			'comment' => 'Комментарий',
			'context' => 'Контекст',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_context',$this->id_context,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('context',$this->context,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
