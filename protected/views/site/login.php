
<div class="errorMessage"></div>
<legend>Login</legend>

<?php if($model->hasErrors()){ ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Ошибка!</h4>
        <p><?=$this->getFirstErrorModel($model)?></p>
    </div>
<?php }?>

<div class="container">
    <div class="row">
        <div class="col-lg-offset-2 col-lg-6">
            <div class="well well-lg">
                <form method="post">
                    <fieldset class="form-horizontal">
                        <div class="form-group">
                            <label for="username" class="col-lg-3 control-label">Пользователь </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="LoginForm[username]" id="username" placeholder="Пользователь..">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-lg-3 control-label">Пароль </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" name="LoginForm[password]" id="password" placeholder="Пароль..">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="rememberMe" name="LoginForm[rememberMe]" value="1">
                                        Запомнить меня
                                    </label>
                                </div>
                                <span class="pull-right">
                                    <button type="submit" name="yt0" class="btn btn-default" >Вход</button>
                                </span>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

