<div class="row">
    <div class="col-lg-12">

    <?php
        $this->pageTitle=Yii::app()->name;
    ?>

    <?php if (Yii::app()->user->isGuest) { ?>
        <div class="jumbotron" style="padding: 24px 34px 24px 30px;">
            <h1><?php echo CHtml::encode(Yii::app()->name); ?></h1>
            <p>Управление планом звонков и SIP номерами</p>
            <p>
                <!--<a class="btn btn-primary btn-large" href="<?=$this->createUrl('/extensions')?>">Расширения</a>-->
            </p>
        </div>
    <?php } ?>

    <?php if (!Yii::app()->user->isGuest) { ?>
        <div class="panel panel-info" id="asterisk-panel-info" style="display: none">
            <div class="panel-heading">
                Asterisk
            </div>

            <div class="panel-body">
                <div class="container" >
                    <div class="row">
                        <div class="col-lg-4">
                            <h4>Состояние: <span class="label" id="asterisk-status"></span></h4>
                            <h4>PID: <span class="label label-info" id="asterisk-pid"></span></h4>
                            <h4>Память: <span class="label label-success" id="asterisk-vmhwm"></span></h4>
                        </div>
                        <div class="col-lg-7" id="aterisk-vmplot" style="height:200px;">
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <button class="btn btn-default" id="btn-asterisk-run">Запустить</button>
                <button class="btn btn-primary" id="btn-asterisk-restart">Перезапустить</button>

                <span class="pull-right">
                    <button class="btn btn-default" id="btn-asterisk-restart-dialplan">Перезагрузить план звонков</button>
                    <button class="btn btn-default" id="btn-asterisk-restart-sip">Перезагрузить SIP</button>
                </span>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                Сервер
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="col-lg-8">
                            <h5>Всего памяти: <span class="badge" class="state-server" id="server-memtotal">0</span></h5>
                            <h5>Свободно памяти: <span class="badge" class="state-server"  id="server-memfree">0</span></h5>
                            <h5>Используется памяти: <span class="badge" class="state-server"  id="server-memusage">0</span></h5>
                        </div>
                        <div class="col-lg-4" id="chart-server-mem" style="height:150px;"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-lg-8">
                            <h5>Swap: <span class="badge" class="state-server"  id="server-swaptotal">0</span></h5>
                            <h5>Swap свободно: <span class="badge" class="state-server"  id="server-swapfree">0</span></h5>
                            <h5>Swap используется: <span class="badge" class="state-server"  id="server-swapusage">0</span></h5>
                        </div>
                        <div class="col-lg-4" id="chart-server-swap" style="height:150px;"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="col-lg-8">
                            <h5>Диск: <span class="badge" class="state-server"  id="server-disktotal">0</span></h5>
                            <h5>Диск свободно: <span class="badge" class="state-server"  id="server-diskfree">0</span></h5>
                            <h5>Диск используется: <span class="badge" class="state-server" id="server-diskusage">0</span></h5>
                        </div>
                        <div class="col-lg-4" id="chart-server-disk" style="height:150px;"></div>
                    </div>
                    <div class="col-lg-6">
                        <h5>CPU(s): <span class="badge"  id="server-cpus">0</span></h5>
                        <div id="server-loadavr"></div>
                    </div>
                </div>
            </div>
        </div>

        <script src="/js/jquery.flot.min.js"></script>
        <script src="/js/jquery.flot.pie.min.js"></script>
        <script src="/js/jquery.flot.time.min.js"></script>
        <script>

            $(document).ready(function(){
                var asteriskExists = <?=$asteriskExists?>;
                var asteriskStarted = 0;
                var updateInterval = 2000;

                var getUrl = function(action){
                    var url = '/site';
                    if (typeof action !== 'undefined')
                        url += '/' + action;

                    return url;
                };

                $("#btn-asterisk-restart").hide();
                $('#btn-asterisk-restart-dialplan').hide();
                $('#btn-asterisk-restart-sip').hide();

                var actionAsterisk = function(action){
                    $.ajax({
                        url:getUrl('/dialplanReload'),
                        data: {action:action},
                        success: function(data){
                            if (data.length>0){
                                $("#flash-message-body").html('<div class="alert alert-warning">'+data+'</div>');
                                $("#flash-message").modal('show');
                            }
                        },
                        async:false
                    });
                };

                $('#btn-asterisk-restart-dialplan').click(function(){
                    actionAsterisk('dr');
                });

                $('#btn-asterisk-restart-sip').click(function(){
                    actionAsterisk('sr');
                });

                $('#btn-asterisk-restart').click(function(){
                    actionAsterisk('restart');
                });

                $('#btn-asterisk-run').click(function(){
                    if (asteriskStarted)
                        actionAsterisk('stop');
                    else
                        actionAsterisk('start');
                });

                var pieOptions = {
                    series: {
                        pie: {
                            show: true
                        }
                    }
                };

                var pieData=[{
                    label: "Используется",
                    color:"rgb(180, 50, 50)",
                    data:0
                },{
                    label: "Свободно",
                    color:"rgb(176, 216, 248)",
                    data:0
                }];

                var plotPie = function(placeholder,first,second){
                    pieData[0].data = first;
                    pieData[1].data = second;
                    $.plot(placeholder,pieData, pieOptions);
                };

                var lenVmData=300;
                var vmData = [];
                var vmhwm=0;
                var plotSettings={
                    series: {
                        lines: {
                            show: true,
                            fill:true
                        },
                        color:"#c4e3f3"
                    },
                    yaxis: {
                        tickFormatter: function (v){
                                            return v + " MB";
                                    },
                        tickDecimals: 0,
                        min: 0,
                        max : 500
                    },
                    xaxis: {
                        mode: 'time',
                        tickSize: [2, 'second'],
                        show: false,
                        min: new Date().getTime()
                    },
                    grid: {
                        backgroundColor: "#282828",
                        tickColor: "#31708f"
                    }
                };

                var processingAsteriskState = function(obj){
                    try {
                        $('#asterisk-status').html(obj.status);

                        if (obj.status=="started"){
                            asteriskStarted = 1;

                            $("#btn-asterisk-run").html("Остановить")
                                .attr('class','btn btn-danger');

                            $("#btn-asterisk-restart").show();
                            $("#asterisk-status").attr('class','label label-success');

                            $('#btn-asterisk-restart-dialplan').show();
                            $('#btn-asterisk-restart-sip').show();

                            $("#asterisk-pid").html(obj.pid);
                            $('#asterisk-vmhwm').html(obj.vmhwm);

                            vmhwm = obj.vmhwmraw;
                        }else{
                            asteriskStarted=0;

                            $("#btn-asterisk-run").html("Запустить")
                                .attr('class','btn btn-success');

                            $("#btn-asterisk-restart").hide();
                            $("#asterisk-status").attr('class','label label-danger');

                            $('#btn-asterisk-restart-dialplan').hide();
                            $('#btn-asterisk-restart-sip').hide();

                            $("#asterisk-pid").html('-');
                            $('#asterisk-vmhwm').html('0');

                            vmhwm=0;
                        }

                        if (vmData.length == lenVmData)
                            vmData.shift();

                        if (vmData.length == 0)
                            plotSettings.yaxis.max = 0;
                        else
                            plotSettings.yaxis.max = Math.max(vmhwm/1024,vmData[vmData.length-1][1]) + 5;

                        vmData.push([new Date().getTime(), vmhwm/1024]);

                        plotSettings.xaxis.min = new Date().getTime() - updateInterval * (lenVmData - 1);
                        plotSettings.xaxis.max = new Date().getTime() ;

                        $.plot("#aterisk-vmplot",[vmData],plotSettings);
                    }catch (err){}
                };

                var processingServerState = function(obj){
                    try {
                        $('#server-memtotal').html(obj.memtotal+' '+obj.sizemem);
                        $('#server-memfree').html(obj.memfree+' '+obj.sizemem);
                        $('#server-memusage').html(obj.memusage+' '+obj.sizemem);

                        $('#server-swaptotal').html(obj.swaptotal+' '+obj.sizeswap);
                        $('#server-swapfree').html(obj.swapfree+' '+obj.sizeswap);
                        $('#server-swapusage').html(obj.swapusage+' '+obj.sizeswap);

                        $('#server-disktotal').html(obj.disktotal+' '+obj.sizedisk);
                        $('#server-diskfree').html(obj.diskfree+' '+obj.sizedisk);
                        $('#server-diskusage').html(obj.diskusage+' '+obj.sizedisk);

                        try{
                            $('#server-cpus').html(obj.cpuinfo.cpus);
                            var progress = '';
                            for(i=0;i<obj.cpuinfo.loadavr.length;i++){
                                progress += '<span style="padding-right: 10px; float: left;  text-align: center;">cpu('+i+'): <b>'+obj.cpuinfo.loadavr[i].toFixed(2)+'%</b> </span><div class="progress"><div class="progress-bar progress-bar-danger" role="progressbar" style="width: '+obj.cpuinfo.loadavr[i]+'%"></div> </div>'
                            }
                            $('#server-loadavr').html(progress);
                        }catch (err){
                            $('#server-loadavr').html('');
                        }

                        plotPie('#chart-server-mem',obj.memusage/obj.memtotal*100,obj.memfree/obj.memtotal*100);
                        plotPie('#chart-server-swap',obj.swapusage/obj.swaptotal*100,obj.swapfree/obj.swaptotal*100);
                        plotPie('#chart-server-disk',obj.diskusage/obj.disktotal*100,obj.diskfree/obj.disktotal*100);
                    }catch (err){
                        $('.state-server').html('0');
                        $('#server-cpus').html('0');
                    }
                };

                var update = function(){
                    $.get(getUrl('getAsteriskState'),processingAsteriskState,'json');
                    $.get(getUrl('getServerState'),processingServerState,'json');

                    setTimeout(update,updateInterval);
                };

                if (asteriskExists){
                    update();
                    $('#asterisk-panel-info').show();
                }
            })
        </script>
    <?php } ?>

    </div>
</div>
