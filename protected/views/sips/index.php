<h1>
    Sip
</h1>
    <table class="table table-striped table-bordered table-condensed table-hover">
        <thead>
            <tr>
                <th>SIP</th>
                <th>Имя</th>
                <th>Контекст</th>
                <th>Mailbox</th>
                <th>Хост</th>
                <th>Тип</th>
                <th>Транспорт</th>
                <th>Локаль</th>
                <th class="text-center" style="width: 120px;" >
                    Действия
                    <button type="button" id="btn-sip-add" class="btn btn-primary btn-xs" title="Добавить SIP">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($sips as $sip){ ?>
                <tr>
                    <td><?=$sip->name?></td>
                    <td><?=$sip->fullname?></td>
                    <td><?=$sip->context?></td>
                    <td><?=$sip->mailbox?></td>
                    <td><?=$sip->host?></td>
                    <td><?=$sip->type?></td>
                    <td><?=$sip->transport?></td>
                    <td><?=$sip->language?></td>
                    <td class="text-center" >
                        <button type="button" id="btn-sip-edit-<?=$sip->id?>" class="btn btn-success btn-xs btn-sip-edit" title="Редактировать">
                            <span class="glyphicon glyphicon-ok"></span>
                        </button>
                        <button type="button" id="btn-sip-remove-<?=$sip->id?>" class="btn btn-danger btn-xs btn-sip-remove" title="Удалить">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
     </table>

    <span class="pull-right">
        <button type="button" class="btn btn-default" id="btn-print-short-sip">Печать</button>
    </span>

<div id="form-edit-sip" class="modal fade"></div>
<div id="form-add-sip" class="modal fade"></div>

<div id="form-confirm" class="modal fade modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="btn-confirm-cancel" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirm-title">Confirm</h4>
            </div>
            <div class="modal-body" id="confirm-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-yes">Да</button>
                <button type="button" class="btn btn-primary" id="btn-confirm-no">Нет</button>
            </div>
        </div>
    </div>
</div>
<div style="height: 60px;"></div>
<script>
    $(document).ready(function(){
        $('#form-edit-sip').modal({show:false});
        $('#form-add-sip').modal({show:false});
        $('#form-confirm').modal({show:false});

        $('#btn-print-short-sip').click(function(){
            $(location).attr('href','/sips/printShortSip');
        });

        function modalConfirm(caption,question,callback)
        {
            $('#confirm-title').html(caption);
            $('#confirm-body').html(question);
            $('#form-confirm').modal('show');

            $('#btn-confirm-no').click(function () {
                $('#form-confirm').modal('hide');
                callback(0);
            })

            $('#btn-confirm-cancel').click(function () {
                $('#form-confirm').modal('hide');
                callback(-1);
            })

            $('#btn-confirm-yes').click(function(){
                $('#form-confirm').modal('hide');
                callback(1);
            });
        }

        $('.btn-sip-remove').bind('click',function(){
            var buttonId=$(this).attr("id");
            var sipId=buttonId.substr(buttonId.lastIndexOf("-")+1);
            modalConfirm("Gamma-asterisk","Вы действительно хотите удалить SIP номер?",function(result){
                if (result==1){
                    $.ajax({
                        url:'/sips/removeSip/id/'+sipId,
                        success:function(data){
                            $(location).attr('href','/sips');
                        },
                        async:false
                    });
                }
            });
        });

        $('.btn-sip-edit').bind('click',function(){
            var buttonId=$(this).attr("id");
            var sipId=buttonId.substr(buttonId.lastIndexOf("-")+1);
            $.ajax({
                url:'/sips/updateSip/id/'+sipId,
                success:function(content){
                    $('#form-edit-sip').html(content);
                    $('#form-edit-sip').modal('show');
                },
                async:false
            });

        });

        $('#btn-sip-add').click(function(){
            $.ajax({
                url: '/sips/addSip',
                success: function(content){
                    $('#form-add-sip').html(content);
                    $('#form-add-sip').modal('show');
                },
                async:false
            });
        });
    });
</script>


