<style>
    td{
        padding: 3px 10px 3px 10px;
    }
    th{
        padding: 5px;
    }
</style>
<table border="1" cellspacing=0>
    <thead>
        <tr>
            <th>SIP</th>
            <th>Имя</th>
            <th>Контекст</th>
            <th>Mailbox</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($sips as $sip) {?>
            <tr>
                <td><?=$sip->name?></td>
                <td><?=$sip->fullname?></td>
                <td><?=$sip->context?></td>
                <td><?=$sip->mailbox?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
