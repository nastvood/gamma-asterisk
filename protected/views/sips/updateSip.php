<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 class="modal-title">Sip</h5>
        </div>
        <div id="edit-sip" class="modal-body">

            <div class="alert alert-danger" id="warn-edit">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <span id="span-edit"></span>
            </div>

            <form id="edit-sips-form" class="form-horizontal">
                <fieldset>
                    <div class="form-group" id="div-n">
                        <label class="col-lg-3 control-label" for="Sips_name" >Sip*</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="Sips_name" value="<?=$model->name?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_fullname" >Имя*</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="Sips_fullname" value="<?=$model->fullname?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_secret" >Пароль*</label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" id="Sips_secret" value="<?=$model->secret?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_context" >Контекст*</label>
                        <div class="col-lg-9">
                            <select class="form-control" id="Sips_context">
                                <?php foreach ($contexts as $context) { ?>
                                    <option <?=($model->context==$context?'selected':'')?> value="<?=$context?>"><?=$context?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_mailbox" >Mailbox</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="Sips_mailbox" value="<?=$model->mailbox?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_host" >Хост*</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="Sips_host" value="<?=$model->host?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_type" >Тип*</label>
                        <div class="col-lg-9">
                            <select class="form-control" id="Sips_type">
                                <?php foreach ($types as $type) { ?>
                                    <option <?=($model->name==$type?'selected':'')?> value="<?=$type?>" ><?=$type?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_transport" >Транспорт*</label>
                        <div class="col-lg-9">
                            <select class="form-control" id="Sips_transport">
                                <?php foreach ($transports as $transport) { ?>
                                    <option <?=($model->transport==$transport?'selected':'')?> value="<?=$transport?>" ><?=$transport?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="Sips_language" >Локализация*</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="Sips_language" value="<?=$model->language?>" />
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btn-save-edit">
                Сохранить изменения
            </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>

<script>
    var idForm='#edit-sips-form';
    var idWarn='#warn-edit';
    var idWarnBody='#span-edit';
    var url='/sips/updateSip/id/<?=$id?>';
    var ajax='sips-form';

    var inputs={
        'Sips_name':{n:'Sips[name]',f:0,e:0},
        'Sips_fullname':{n:'Sips[fullname]',f:0,e:0},
        'Sips_secret':{n:'Sips[secret]',f:0,e:0},
        'Sips_context':{n:'Sips[context]',f:0,e:0},
        'Sips_host':{n:'Sips[host]',f:0,e:0},
        'Sips_mailbox':{n:'Sips[mailbox]',f:0,e:0},
        'Sips_type':{n:'Sips[type]',f:0,e:0},
        'Sips_transport':{n:'Sips[transport]',f:0,e:0},
        'Sips_language':{n:'Sips[language]',f:0,e:0}
    };

    $(idWarn).hide();

    function getInputData()
    {
        var data={};
        for (var key in inputs) {
            data[inputs[key].n]=$(idForm+' #'+key).val();
        }

        return data;
    }

    function errorsClear(data){
        try{
            for (var key in data){
                data[key].e=0;
            }
        }catch (err){}
    }

    $(idForm+' input').focusout(function(e){
        var curElemId=$(e.target).attr('id');
        inputs[curElemId].f=1;
        var data=getInputData();
        data['ajax']=ajax;
        $.post(url,
            data,
            function(data){
                try{
                    $(idWarn).show();
                    $(idWarnBody).html('');
                    obj = JSON.parse(data);
                    errorsClear(inputs);
                    for (var key in obj) {
                        if (inputs[key].f==1){
                            $(idWarnBody).append('<p><span class="glyphicon glyphicon-warning-sign"> </span>'+obj[key]+'</p>');
                            var formGroup=$('#'+key).closest(".form-group");
                            formGroup.removeClass("has-success").addClass("has-error");
                            inputs[key].e=1;
                        }
                    }
                    for (var key in inputs){
                        if ((inputs[key].e==0) && (inputs[key].f==1)){
                            var formGroup=$('#'+key).closest(".form-group");
                            formGroup.removeClass("has-error").addClass("has-success");

                        }
                    }
                    if ($(idWarnBody).html()==''){
                        //$(e.target).parent().removeClass("has-error").addClass("has-success");
                        $(idWarn).hide();
                    }
                }catch (err){
                    $(idWarn).hide();
                }
            }
        );
    });

    $('#btn-save-edit').click(function(){
        $.post(url,
            getInputData(),
            function(data){
                try{
                    obj = JSON.parse(data);
                    if (obj.result==1){
                        $(location).attr('href','/sips');
                    }else{
                        $(idWarn).show();
                        $(idWarnBody).html('<p><span class="glyphicon glyphicon-warning-sign"> </span>'+obj.error+'</p>');
                    }
                }catch (err){}
            });
    });
</script>



