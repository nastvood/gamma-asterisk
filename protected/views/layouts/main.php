<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="ru" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css" media="screen" />

        <link href="/images/login.png" rel="icon" type="image/x-icon">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <?php
            $controller=Yii::app()->controller->id;
            $action=Yii::app()->controller->action->id;
        ?>
        <nav class="navbar-inverse navbar-fixed-top navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?=$this->createUrl('/site')?>">
                        <!--<img style="margin-right: 3px" width="13px" alt src="/images/login.png">-->
                        <?php echo CHtml::encode(Yii::app()->name); ?>
                    </a>
                </div>
                <div class="collapse navbar-collapse" >
                    <ul class="navbar-nav navbar-right nav">
                        <li <?=(($controller=='site')&&($action=='index'))?'class="active"':'' ?> >
                            <a href="<?=$this->createUrl('/site')?>" >Главная</a>
                        </li>
                        <?php if (Yii::app()->user->isGuest) { ?>
                            <li <?=(($controller=='site')&&($action=='login'))?'class="active"':'' ?>>
                                <a href="<?=$this->createUrl('/site/login')?>">Вход</a>
                            </li>
                        <?php } else {?>
                            <li <?=(($controller=='extensions')&&($action=='index'))?'class="active"':'' ?>>
                                <a href="<?=$this->createUrl('/extensions')?>">План звонков</a>
                            </li>
                            <li class="<?=($controller=='conf')?'active':''?> dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Конфигурация<b class="caret" ></b></a>
                                <ul class="dropdown-menu">
                                    <li >
                                        <a href="<?=$this->createUrl('/conf/index',array('c'=>AsteriskConfigs::ASTERISK))?>">asterisk.conf</a>
                                    </li>
                                    <li>
                                        <a href="<?=$this->createUrl('/conf/index',array('c'=>AsteriskConfigs::CHAN_DAHDY))?>" >chan_dahdi.conf</a>
                                    </li>
                                    <li >
                                        <a href="<?=$this->createUrl('/conf/index',array('c'=>AsteriskConfigs::EXTCONFIG))?>">extconfig.conf</a>
                                    </li>
                                    <li >
                                        <a href="<?=$this->createUrl('/conf/index',array('c'=>AsteriskConfigs::FEATURES))?>">features.conf</a>
                                    </li>
                                    <li >
                                        <a href="<?=$this->createUrl('/conf/index',array('c'=>AsteriskConfigs::MODULES))?>">modules.conf</a>
                                    </li>
                                    <li >
                                        <a href="<?=$this->createUrl('/conf/index',array('c'=>AsteriskConfigs::SIP))?>">sip.conf</a>
                                    </li>
                                </ul>
                            </li>
                            <li <?=(($controller=='sips')&&($action=='index'))?'class="active"':'' ?> >
                                <a href="<?=$this->createUrl('/sips')?>">SIP</a>
                            </li>
                            <li <?=(($controller=='site')&&($action=='cdr'))?'class="active"':'' ?> >
                                <a href="<?=$this->createUrl('/site/cdr')?>">CDR</a>
                            </li>
                            <li <?=(($controller=='site')&&($action=='logout'))?'class="active"':'' ?> >
                                <a href="<?=$this->createUrl('/site/logout')?>">Выход(<?=Yii::app()->user->name?>)</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container" id="page">
            <div class="row" style="height: 60px;"></div>
            <div class="row">
                <?php echo $content; ?>
            </div>
            <div class="row" style="height: 60px;">
            <div class="navbar navbar-fixed-bottom navbar-inverse">
                <div class="container">
                    <div class="collapse navbar-collapse" style="text-align: center">
                        <p class="navbar-text" style="width: 100%">
                            Copyright &copy; <?php echo date('Y'); ?> ОАО Гамма.
                            <br/>
                            <?php echo Yii::powered(); ?>
                            /<a href="http://getbootstrap.com">Bootstrap</a>
                            /<a href="http://knockoutjs.com">knockoutjs</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--[if lt IE 8]>
            <script type="text/javascript" src="/js/css3-mediaqueries.js" />
        <![endif]-->

        <?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
        <script src="/js/bootstrap.min.js" ></script>
        <script src="/js/knockout.js" ></script>

        <!--Flash message -- begin-->
        <div id="flash-message" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Сообщение</h4>
                    </div>
                    <div class="modal-body" id="flash-message-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">ОК</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $("#flash-message").modal({show:false});

            <?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
                $("#flash-message-body").append(
                    '<div class="alert alert-<?=$key?>"><?=CHtml::encode($message)?>'
                );
                $("#flash-message").modal('show')
            <?php } ?>
        </script>
        <!--Flash message -- end-->
    </body>
</html>
