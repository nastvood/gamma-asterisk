<div class="container">
    <div class="row">
        <div class="page-header">
            <h1>План звонков</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Контексты
                    <span class="badge" style="margin-left: 5px" data-bind="text:contexts().length"></span>
                    <button class="btn btn-warning btn-xs pull-right" title="Обновить контексты" data-bind="click:$root.getContexts">
                        <span class="glyphicon glyphicon-refresh"></span>
                    </button>
                    <button class="btn btn-success btn-xs pull-right" title="Новый контекст" style="margin-right: 5px" data-bind="click:$root.showDialogNewContext">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </div>
                <div class="panel-body" style="border-bottom: 1px solid #ddd;padding: 6px 10px">
                    <input type="text" class="form-control" placeholder="Поиск" data-bind="value:searchContext" style="height: 30px"/>
                </div>

                <ul id='ul-contexts' class="list-group" style="min-height: 100px;overflow: auto">
                    <!-- ko foreach: contexts -->
                        <li>
                            <a class="list-group-item " style="padding: 8px 8px;cursor: pointer"
                                data-bind="text:$data.context,click:$root.contextClick,css:{'list-group-item-info':$data.context == $root.curContext().context}"></a>
                        </li>
                     <!-- /ko -->
                </ul>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="panel panel-default" data-bind="with:curContext">
                <div class="panel-heading">
                    Контекст: <b data-bind="text:context"></b>
                    <span class="pull-right">
                        <div class="btn-group btn-group-sm">
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Контекст <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" data-bind="click:$root.showDialogNewContext">Новый</a></li>
                                    <li><a href="#" data-bind="click: $root.showDialogEditContext">Редактировать</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" data-bind="click: $root.removeContext">Удалить</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Расширения <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" data-bind="click: $root.showDialogNewExt">Новое</a></li>
                                    <li><a href="#" data-bind="click: $root.showDialogEditExt">Редактировать</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" data-bind="click: $root.removeExt">Удалить</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body" data-bind="text:comment">
                </div>
            </div>
            <ul class="nav nav-tabs " id="tab-exts" data-bind="foreach: exts">
                <li data-bind="css:$root.getExtTabContentActive($data)">
                    <a data-toggle="tab" data-bind="text:$data.ext,attr:{href:'#'+$root.getIdForExtTab($data)},click:$root.clickTabExt"></a>
                </li>
            </ul>

            <div class="tab-content" data-bind="foreach: exts">
                <div data-bind="attr:{id:$root.getIdForExtTab($data)},css:'tab-pane '+$root.getExtTabContentActive($data)">
                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th style="vertical-align: middle;text-align: center;">Приоритет</th>
                                <th style="vertical-align: middle;text-align: center;">Команда</th>
                                <th style="vertical-align: middle;text-align: center;">Параметры</th>
                                <th class="text-center" style="vertical-align: middle;text-align: center;">
                                    Действия
                                    <button class="btn btn-xs btn-primary" data-bind="click:$root.showDialogNewApp" title="Добавить команду" style="margin-left: 5px">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach:apps">
                            <tr>
                                <td style="width: 50px;" data-bind="text:$data.priority"></td>
                                <td style="width: 120px" data-bind="text:$data.app"></td>
                                <td data-bind="text:$data.appdata"></td>
                                <td class="text-center" style="width: 200px">
                                    <button type="button" class="btn btn-success btn-xs btn-app-edit" data-bind="click:$root.showDialogEditApp" title="Редактировать">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </button>
                                    <button type="button" class="btn btn-default btn-xs btn-app-priority-up "
                                            data-bind='click:function(data, event){$root.changePriorityApp(data,$index(),-1)},css:{ "disabled":($index()==0)}'
                                            title="Поднять приоритет">
                                        <span class="glyphicon glyphicon-arrow-up"></span>
                                    </button>
                                    <button type="button" class="btn btn-default btn-xs btn-app-priority-down "
                                            data-bind='click:function(data, event){$root.changePriorityApp(data,$index(),1)},css:{ "disabled":($index()==($parent.apps.length-1))}'
                                            title="Опустить приоритет">
                                        <span class="glyphicon glyphicon-arrow-down"></span>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-xs btn-app-remove" data-bind="click:$root.removeApp" title="Удалить">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modals"></div>

<script type="text/javascript" src="/js/gforms.js"></script>
<script>
    $(document).ready(function(){
        var appName='Gamma-asterisk';

        var createUrl = function(action,params){
            var url = '/jsonext';

            if (typeof action !== 'undefined'){
                url += '/' + action;
            }

            if (typeof params !== 'undefined'){
                if (params instanceof Object){
                    for (var key in params){
                        url += '/' + key + '/' + params[key]
                    }
                }
            }

            return url;
        };

        var resizeWindow = function(){
            $('#ul-contexts').css('max-height',$(window).height() - 400);
        };

        $(window).resize(resizeWindow);

        resizeWindow();

        $.ajax({
            url:'<?=Yii::app()->createUrl('exts/modals')?>',
            async:false,
            dataType:'html',
            success:function(data){
                $('#modals').replaceWith(data);
            }
        });

        function Context(){
            var self=this;

            self.idContext=0;
            self.context='';
            self.comment='';
        }

        function Ext(){
            var self=this;

            self.idExt=0;
            self.context=0;
            self.ext='';
            self.apps=[];
        }

        function App(){
            var self=this;

            self.idApp=0;
            self.ext=0;
            self.priority=0;
            self.app='';
            self.appdata='';
        }

        function ContextsViewModel(){
            var self=this;

            self.rawContexts=ko.observableArray();
            self.curContext=ko.observable();
            self.editContext=ko.observable();
            self.newContext=ko.observable();
            self.exts=ko.observableArray();
            self.curExt=ko.observable();
            self.newExt=ko.observable();
            self.editExt=ko.observable();
            self.apps=ko.observableArray();
            self.newApp=ko.observable();
            self.editApp=ko.observable();
            self.asteriskApps=ko.observableArray();
            self.searchContext=ko.observable();
            self.contexts=ko.computed(function(){
                var search='';
                if (typeof self.searchContext() !== 'undefined'){
                    search= $.trim(self.searchContext());
                }
                search = search.toUpperCase();

                return ko.utils.arrayFilter(self.rawContexts(),function(context){
                    if (search === '')
                        return true;

                    if (context.context.toUpperCase().indexOf(search) > -1)
                        return true;

                    return false;
                });
            });

            var formEditContext=new YiiForm(
                'modal-edit-context',
                'Contexts',
                function(){
                    return createUrl('EditContext',{context:self.curContext().idContext});
                },
                function(){
                    return createUrl('EditContext',{context:self.curContext().idContext});
                }
            );

            var formNewContext=new YiiForm(
                'modal-new-context',
                'Contexts',
                function(){
                    return createUrl('NewContext');
                },
                function(){
                    return createUrl('NewContext');
                }
            );

            var formNewExt=new YiiForm(
                'modal-new-ext',
                'Exts',
                function(){
                    return createUrl('NewExt');
                },
                function(){
                    return createUrl('NewExt');
                }
            );

            var formEditExt=new YiiForm(
                'modal-edit-ext',
                'Exts',
                function(){
                    return createUrl('EditExt',{ext:self.curExt().idExt});
                },
                function(){
                    return createUrl('EditExt',{ext:self.curExt().idExt});
                }
            );

            var formNewApp=new YiiForm(
                'modal-new-app',
                'Apps',
                function(){
                    return createUrl('newApp');
                },
                function(){
                    return createUrl('newApp');
                }
            );

            var formEditApp=new YiiForm(
                'modal-edit-app',
                'Apps',
                function(){
                    return createUrl('editApp',{app:self.editApp().idApp});
                },
                function(){
                    return createUrl('editApp',{app:self.editApp().idApp});
                }
            );

            self.indexOfContexts=function(idContext){
                var index=-1;
                var match=ko.utils.arrayFirst(self.rawContexts(),function(context){
                    index++;
                    return (context.idContext==idContext)
                });

                if (match)
                    return index;
                else
                    return -1;
            };

            self.indexOfExts=function(idExt){
                var index=-1;
                var match=ko.utils.arrayFirst(self.exts(),function(ext){
                    index++;
                    return (ext.idExt == idExt)
                });

                if (match)
                    return index;
                else
                    return -1;
            };

            self.indexOfApps=function(apps,idApp){
                var index=-1;
                var match=ko.utils.arrayFirst(apps,function(app){
                    index++;
                    return (app.idApp==idApp)
                });

                if (match)
                    return index;
                else
                    return -1;
            };

            self.removeContext=function(context){
                gConfirmMessage.confirm(appName,'Удалить контекст <b>'+context.context+'</b>?',function(result){
                    if (result<1)
                        return;

                    $.ajax({
                        url:createUrl('removeContext',{context:context.idContext}),
                        success:function(data){
                            try{
                                if(data.err==0){
                                    var index=self.indexOfContexts(context.idContext);
                                    if (index>-1){
                                        self.rawContexts.remove(context);
                                        var newIndex=(index==self.rawContexts().length?index-1:index);
                                        if (typeof self.rawContexts()[newIndex]!=="undefined"){
                                            self.curContext(self.rawContexts()[newIndex]);
                                        }
                                    }

                                    gMessages.showSuccess(appName,'Контекст удален!');
                                }else{
                                    gMessages.showDanger(appName,'Ошибка: '+data.errMsg);
                                }
                            }catch (e){
                                gMessages.showDanger(appName,'Неизвестная ошибка!');
                            }
                        },
                        dataType:'json',
                        async:false
                    });
                });
            }

            self.sortContext=function(left,right){
                var leftContext=left.context.toLowerCase();
                var rightContext=right.context.toLowerCase();
                return leftContext == rightContext ? 0 : (leftContext < rightContext ? -1 : 1)
            }

            self.sortExt=function(left,right){
                var leftExt=left.ext.toLowerCase();
                var rightExt=right.ext.toLowerCase();
                return leftExt == rightExt ? 0 : (leftExt < rightExt ? -1 : 1)
            }

            self.contextClick=function(context){
                self.exts(null);
                self.curExt(null);
                self.apps(null);

                self.curContext(context);

                $.ajax({
                    url:createUrl('context'),
                    data:{'context':context.idContext},
                    success:function(data){
                        if (data.length>0){
                            self.exts(data);
                            self.exts.sort(self.sortExt);
                            self.apps(data[0].apps);
                            self.curExt(data[0]);
                        }else{
                            self.exts([])
                            self.apps([]);
                        }
                    },
                    dataType:'json',
                    async:false
                });
            }

            self.getIdForExtTab=function(ext){
                return 'tab-ext-'+ext.idExt;
            }

            self.getExtTabContentActive=function(ext){
                if (self.curExt()){
                    if (ext.idExt==self.curExt().idExt)
                        return 'active';
                }

                return '';
            }

            self.clickTabExt=function(ext){
                self.curExt(ext);
                self.apps(ext.apps);
            }

            self.getContexts = function(){
                $.ajax({
                    url:createUrl('contexts'),
                    success:self.rawContexts,
                    dataType:'json',
                    async:false
                });

                if (self.rawContexts().length>0){
                    self.rawContexts.sort(self.sortContext);

                    var index = 0;
                    if (typeof self.curContext() !== 'undefined'){
                        index = self.indexOfContexts(self.curContext().idContext)
                        if (index < 0)
                            index = 0
                    }

                    self.contextClick(self.rawContexts()[index]);
                }
            };

            self.showDialogNewContext=function(){
                self.newContext(new Context());
                formNewContext.open();
            };

            self.saveNewContext=function(context){
                if (formNewContext.save()){
                    formNewContext.close();

                    context.idContext=formNewContext.getSaveId();
                    self.rawContexts.push(context);
                    self.rawContexts.sort(self.sortContext);
                    self.curContext(self.rawContexts()[self.indexOfContexts(context.idContext)]);
                    self.contextClick(self.curContext());

                    gMessages.showSuccess('Gamma-asterisk',formNewContext.getSaveMessage());
                }
            };

            self.closeDialogNewContext=function(){
                formNewContext.close();
            };

            self.showDialogEditContext=function(){
                self.copyObjectJs(self.curContext,self.editContext);
                formEditContext.open();
            };

            self.saveEditContext=function(context){
                if (formEditContext.save()){
                    var index=self.indexOfContexts(self.curContext().idContext);
                    gMessages.showSuccess('Gamma-asterisk','Контекст сохранён.');
                    self.copyObjectJs(self.editContext,self.curContext);
                    self.rawContexts()[index]=self.curContext();
                    self.rawContexts.replace(self.rawContexts()[index],self.curContext());
                    self.rawContexts.sort(self.sortContext);

                    formEditContext.close();
                }
            };

            self.closeEditContext=function(context){
                formEditContext.close();
            };

            self.getIdCurContext=function(){
                if (self.curContext())
                    return self.curContext().idContext;
                else
                    return 0;
            };

            self.showDialogNewExt=function(context){
                var ext=new Ext();
                ext.context=context.idContext;
                self.newExt(ext);

                formNewExt.setTitle('Новое расширение для контекста <b>'+context.context+'</b>');
                formNewExt.open();
            };

            self.closeDialogNewExt=function(ext){
                formNewExt.close();
            };

            self.saveNewExt=function(ext){
                if (formNewExt.save()){
                    formNewExt.close();

                    ext.idExt=formNewExt.getSaveId();
                    ext.idContext=self.curContext().idContext;
                    self.exts.push(ext);
                    self.clickTabExt(self.exts()[self.exts().length-1]);
                    self.exts.sort(self.sortExt);

                    gMessages.showSuccess(appName,formNewExt.getSaveMessage());
                }
            };

            self.showDialogEditExt=function(context){
                if (self.curExt()==null)
                    return;

                self.copyObjectJs(self.curExt,self.editExt);
                formEditExt.setTitle('Редактировать расширение <b>'+self.curExt().ext+'</b>'+' контекста <b>'+context.context+'</b>');
                formEditExt.open();
            };

            self.closeEditExt=function(ext){
                formEditExt.close();
            };

            self.saveEditExt=function(ext){
                if (formEditExt.save()){
                    formEditExt.close();

                    var index=self.indexOfExts(self.curExt().idExt);
                    if (index<0)
                        return;

                    self.copyObjectJs(self.editExt,self.curExt);
                    self.exts.replace(self.exts()[index],self.curExt());
                    self.exts.sort(self.sortExt);

                    gMessages.showSuccess(appName,formEditExt.getSaveMessage());
                }
            };

            self.removeExt=function(context){
                var mess = 'Удалить расширение <b>'+self.curExt().ext+'</b> контекста<b>'+context.context+'</b>?'
                gConfirmMessage.confirm(appName,mess,function(result){
                    if (result<1)
                        return;

                    var successCallback=function(data){
                        try{
                            if(data.err==0){
                                var index=self.indexOfExts(self.curExt().idExt);
                                if (index>-1){
                                    self.exts.remove(self.exts()[index]);
                                    if (self.exts().length>0){
                                        var newIndex=(index==self.exts().length?index-1:index);
                                        if (typeof self.exts()[newIndex]!=="undefined"){
                                            self.curExt(self.exts()[newIndex]);
                                        }
                                    }
                                }
                                gMessages.showSuccess(appName,'Расширение удалено!');
                            }else{
                                gMessages.showDanger(appName,'Ошибка: '+data.errMsg);
                            }
                        }catch (e){
                            gMessages.showDanger(appName,'Неизвестная ошибка.');
                        }
                    }

                    $.ajax({
                        url:createUrl('removeExt'),
                        data:{ext:self.curExt().idExt},
                        success:successCallback,
                        dataType:'json',
                        async:false
                    });
                });
            };

            self.removeApp=function(app){
                var mess = 'Удалить команду <b>'+app.app+'</b> расширения <b>'
                    + self.curExt().ext+'</b> контекста <b>'+self.curContext().context+'</b>?';

                gConfirmMessage.confirm(appName,mess,function(result){
                    if (result<1)
                        return;

                    var successCallback=function(data){
                        try{
                            if(data.err==0){
                                var extIndex=self.indexOfExts(app.ext);
                                if (extIndex<0)
                                    return;

                                var curExt=self.exts()[extIndex];
                                var appIndex=self.indexOfApps(curExt.apps,app.idApp);
                                if (appIndex<0)
                                    return;

                                self.curExt().apps.splice(appIndex,1);

                                for(var i=appIndex;i<self.curExt().apps.length;i++){
                                    self.curExt().apps[i].priority-=1;
                                }

                                self.curExt(curExt);
                                self.apps(self.curExt().apps);

                                self.exts.splice(extIndex,1);
                                if (self.exts().length>0)
                                    self.exts.splice(extIndex,0,self.curExt());
                                else
                                    self.exts.push(self.curExt());

                                if (self.curExt().apps.length==0)
                                    self.removeExt(self.curContext());

                                gMessages.showSuccess(appName,'Кломанда удалена!');
                            }else{
                                gMessages.showDanger(appName,'Ошибка: '+data.errMsg);
                            }
                        }catch (e){
                            gMessages.showDanger(appName,'Неизвестная ошибка.');
                        }
                    }

                    $.ajax({
                        url:createUrl('removeApp'),
                        data:{app:app.idApp},
                        success:successCallback,
                        dataType:'json',
                        async:false
                    });
                });
            };

            self.showDialogNewApp=function(ext){
                var app=new App();
                app.ext=ext.idExt;
                self.newApp(app);
                formNewApp.setTitle('Новое команда для расширения: <b>'+ext.ext+'</b> контекста <b>'+self.curContext().context+'</b> ');
                formNewApp.open();
            };

            self.closeNewApp=function(ext){
                formNewApp.close();
            };

            self.saveNewApp=function(app){
                if (formNewApp.save()){
                    formNewApp.close();

                    var extIndex=self.indexOfExts(app.ext);
                    if (extIndex<0)
                        return;

                    var savedData=formNewApp.getSaveData().data;
                    app.app=savedData.app;
                    app.appdata=savedData.appdata;
                    app.idApp=formNewApp.getSaveId();
                    app.priority=savedData.priority;

                    self.curExt().apps.push(app);
                    self.apps(self.curExt().apps);

                    self.exts.splice(extIndex,1);
                    self.exts.splice(extIndex,0,self.curExt());

                    gMessages.showSuccess(appName,formNewApp.getSaveMessage());
                }
            };

            self.closeEditExt=function(ext){
                formEditExt.close();
            };

            self.showDialogEditApp=function(app){
                self.editApp(app);
                formEditApp.setTitle('Редактировать команду <b>'+app.app+'</b> расширение <b>'+self.curExt().ext+'</b>'+' контекста <b>'+self.curContext().context+'</b>');
                formEditApp.open();
            };

            self.closeEditApp=function(app){
                formEditApp.close();
            };

            self.saveEditApp=function(app){
                if (formEditApp.save()){
                    formEditApp.close();

                    var extIndex = self.indexOfExts(app.ext);
                    if (extIndex<-1)
                        return;

                    var appIndex=self.indexOfApps(self.curExt().apps,app.idApp);
                    if (appIndex<0)
                        return;

                    self.apps.replace(self.apps()[appIndex],app);

                    self.exts.splice(extIndex,1);
                    self.exts.splice(extIndex,0,self.curExt());

                    gMessages.showSuccess(appName,formEditApp.getSaveMessage());
                }
            };

            self.changePriorityApp = function(app,index,offsetPos){
                var extIndex = self.indexOfExts(app.ext);
                if (extIndex<-1)
                    return;

                var successCallback = function(data){
                    if(data.err==0){
                        var oldPriority = self.curExt().apps[index].priority;
                        var newPriority = self.curExt().apps[index + offsetPos].priority;
                        self.curExt().apps[index + offsetPos].priority = oldPriority;
                        self.curExt().apps[index].priority = newPriority;
                        var oldApp = self.curExt().apps[index + offsetPos];
                        self.curExt().apps[index + offsetPos] = self.curExt().apps[index];
                        self.curExt().apps[index] = oldApp;

                        self.apps(self.curExt().apps);

                        self.exts.splice(extIndex,1);
                        self.exts.splice(extIndex,0,self.curExt());
                    }else{
                        gMessages.showDanger(appName,'Ошибка: '+data.errMsg);
                    }
                }

                $.ajax({
                    url:createUrl('ChangePriorityApp'),
                    data: {
                        id1:self.curExt().apps[index].idApp,
                        id2:self.curExt().apps[index + offsetPos].idApp
                    },
                    async:false,
                    dataType:'json',
                    success:successCallback
                });
            };

            self.copyObjectJs=function(srcObj,destObj){
                var srcObjJS=srcObj();
                var destObjJS={};
                for(var key in srcObjJS){
                    destObjJS[key]=srcObjJS[key];
                }
                destObj(destObjJS);
            };

            $.ajax({
                url:createUrl('asteriskapps'),
                success:self.asteriskApps,
                dataType:'json',
                async:false
            });

            self.getContexts();
        }

        var contextsViewModel=new ContextsViewModel();
        ko.applyBindings(contextsViewModel);
    });
</script>

