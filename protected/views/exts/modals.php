﻿<?php header('Content-Type: text/html; charset=utf-8'); ?>

<div class="modal fade" id="modal-message">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissable">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="alert alert-success alert-dismissable">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="modal-confirm-btn-yes">Да</button>
                <button type="button" class="btn btn-default" id="modal-confirm-btn-no">Нет</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-new-context" data-bind="with: newContext">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новый контекст</h4>
            </div>
            <div class="modal-body" id="form-modal-new-context">
                <div class="alert alert-danger alert-dismissable">
                    <span class="modal-errors"></span>
                </div>
                <form role="form">
                    <div class="form-group">
                        <label>Контекст</label>
                        <input class="form-control" name="context" data-bind="value: context">
                    </div>
                    <div class="form-group">
                        <label>Комментарий</label>
                        <input class="form-control" name="comment" data-bind="value: comment">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bind="click:$root.saveNewContext">Сохранить</button>
                <button type="button" class="btn btn-default" data-bind="click:$root.closeDialogNewContext">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-context" data-bind="with: editContext">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bind="click:$root.closeEditContext" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><b>Редактирова контекст:</b> <span data-bind="text: context"></span></h4>
            </div>
            <div class="modal-body" id="form-modal-edit-context">
                <div class="alert alert-danger alert-dismissable">
                    <span class="modal-errors"></span>
                </div>
                <form role="form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id_context" data-bind="value: idContext" />
                    </div>
                    <div class="form-group">
                        <label>Контекст</label>
                        <input class="form-control" name="context" data-bind="value: context">
                    </div>
                    <div class="form-group">
                        <label>Комментарий</label>
                        <input class="form-control" name="comment" data-bind="value: comment">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bind="click:$root.saveEditContext">Сохранить</button>
                <button type="button" class="btn btn-default" data-bind="click:$root.closeEditContext">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-new-ext" data-bind="with: newExt">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bind="click:$root.closeDialogNewExt" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новое расширение для контекста</h4>
            </div>
            <div class="modal-body" id="form-modal-new-ext">
                <div class="alert alert-danger alert-dismissable">
                    <span class="modal-errors"></span>
                </div>
                <form role="form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="context" data-bind="value: context">
                    </div>
                    <div class="form-group">
                        <label>Расширение</label>
                        <input class="form-control" name="ext" data-bind="value: ext">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bind="click:$root.saveNewExt">Сохранить</button>
                <button type="button" class="btn btn-default" data-bind="click:$root.closeDialogNewExt">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-ext" data-bind="with: editExt">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bind="click:$root.closeEditExt" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="form-modal-edit-ext">
                <div class="alert alert-danger alert-dismissable">
                    <span class="modal-errors"></span>
                </div>
                <form role="form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id_ext" data-bind="value: idExt" />
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id_context" data-bind="value: idContext" />
                    </div>
                    <div class="form-group">
                        <label>Расширение</label>
                        <input class="form-control" name="ext" data-bind="value: ext">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bind="click:$root.saveEditExt">Сохранить</button>
                <button type="button" class="btn btn-default" data-bind="click:$root.closeEditExt">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-new-app" data-bind="with: newApp">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bind="click:$root.closeNewApp" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="form-modal-new-app">
                <div class="alert alert-danger alert-dismissable">
                    <span class="modal-errors"></span>
                </div>
                <form role="form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="ext" data-bind="value: ext" />
                    </div>
                    <div class="form-group">
                        <label>Команда</label>
                        <select class="form-control" name="app" data-bind="options:$root.asteriskApps">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Данные</label>
                        <input class="form-control" name="appdata" data-bind="value: appdata">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bind="click:$root.saveNewApp">Сохранить</button>
                <button type="button" class="btn btn-default" data-bind="click:$root.closeNewApp">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-app" data-bind="with: editApp">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bind="click:$root.closeEditApp" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="form-modal-edit-app">
                <div class="alert alert-danger alert-dismissable">
                    <span class="modal-errors"></span>
                </div>
                <form role="form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id_app" data-bind="value: idApp" />
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="ext" data-bind="value: ext" />
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="priority" data-bind="value: priority" />
                    </div>
                    <div class="form-group">
                        <label>Команда</label>
                        <select class="form-control" name="app" data-bind="options:$root.asteriskApps,value:app">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Данные</label>
                        <input class="form-control" name="appdata" data-bind="value: appdata">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bind="click:$root.saveEditApp">Сохранить</button>
                <button type="button" class="btn btn-default" data-bind="click:$root.closeEditApp">Закрыть</button>
            </div>
        </div>
    </div>
</div>