<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Extension: <?=$model->context?>, Контекст: <?=$model->exten?></h4>
        </div>
        <div id="add_app" class="modal-body">

            <div class="alert alert-danger" id="warn_add">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <span id="span_add"></span>
            </div>

            <form id="add-extensions-form">
                <fieldset>
                    <input type="hidden" id="Extensions_context" value="<?=$model->context?>" />
                    <input type="hidden" id="Extensions_exten" value="<?=$model->exten?>" />
                    <div class="form-group">
                        <label class="control-label" for="Extensions_priority">Приоритет: <?=$model->priority?></label>
                        <input type="hidden" class="form-control" id="Extensions_priority" value="<?=$model->priority?>" />
                    </div>
                    <div class="form-group" id="form-group-exten">
                        <label class="control-label" for="Extensions_app" >Команда*</label>
                        <select class="form-control" id="Extensions_app">
                            <?php foreach($asteriskApps as $app) { ?>
                                <option><?=$app?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="Extensions_appdata">Параметры</label>
                        <input type="text" class="form-control" id="Extensions_appdata" value="<?=$model->appdata?>" />
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btn_save_add">
                Сохранить изменения
            </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>

<script>
    var idForm='#add-extensions-form';
    var idWarn='#warn_add';
    var idWarnBody='#span_add';
    var url='/extensions/addApp/exten/';
    var ajax='extensions-form';

    var inputs={
        'Extensions[context]':'Extensions_context',
        'Extensions[exten]':'Extensions_exten',
        'Extensions[priority]':'Extensions_priority',
        'Extensions[app]':'Extensions_app',
        'Extensions[appdata]':'Extensions_appdata'
    };

    $(idWarn).hide();

    function getInputData()
    {
        var data={};
        for (var key in inputs) {
            data[key] = $(idForm+' #'+inputs[key]).val();
        }

        return data;
    }

    $(idForm+' input').focusout(function(e){
        var data = getInputData();
        data['ajax'] = ajax;
        $.post(url + <?=$exten?>,
            data,
            function(data){
                try{
                    $(idWarn).show();
                    $(idWarnBody).html('');
                    obj = JSON.parse(data);
                    for (var key in obj) {
                        $(idWarnBody).append('<p><span class="glyphicon glyphicon-warning-sign"> </span>' + obj[key] + '</p>');
                        $(key).parent().removeClass("has-success").addClass("has-error");
                    }
                    if ($(idWarnBody).html()==''){
                        $(e.target).parent().removeClass("has-error").addClass("has-success");
                        $(idWarn).hide();
                    }
                }catch (err){
                    $(idWarn).hide();
                }
            }
        );
    });

    $('#btn_save_add').click(function(){
        $.post(url + <?=$exten?>,
            getInputData(),
            function(data){
                try{
                    obj = JSON.parse(data);
                    if (obj.result==1){
                        $(location).attr('href','/extensions/index/id/<?=$exten?>');
                    }else{
                        $(idWarnBody).append('<p><span class="glyphicon glyphicon-warning-sign"> </span>' + obj.error + '</p>');
                    }
                }catch (err){}
            });
    });
</script>



