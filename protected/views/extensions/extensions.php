<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
<script>
    $(document).ready(function(){
        $("#accordion-<?=$contextId ?>").accordion({
            header:'h3',
            clearStyle: true,
            create:function(event, ui){
                exten=$(ui.header).text();
                $.get("/extensions/apps/context/<?=$context?>/exten/"+exten,function(content){
                    $("div#div-<?=$contextId?>-"+ $.md5(exten)).html(content);
                });
            },
            change:function(event, ui){
                var active=$("#accordion-<?=$contextId ?>").accordion("option","active");
                exten=$("#accordion-<?=$contextId ?> h3").eq(active).text();
                $.get("/extensions/apps/context/<?=$context?>/exten/"+exten,function(content){
                    $("div#div-<?=$contextId?>-"+ $.md5(exten)).html(content);
                });
            }
        });
    });
</script>

<div id="accordion-<?=$contextId ?>" >
    <?php foreach($extensions as $extension){ ?>
        <h3><?=$extension->exten;?><?=$extension->id;?></h3>
        <div id="div-<?=$contextId ?>-<?=md5($extension->exten)?>">
            <?php print_r($extension->group)?>
        </div>
    <?php } ?>
</div>
<?php print_r($extensions); ?>
