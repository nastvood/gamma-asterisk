<?php if ($apps) { ?>
<br>
<h3>
    Расширение: <?=$apps[0]->exten?>
    <button type="button" id="btn_app_add_<?=$apps[0]->id?>" class="btn btn-primary btn-small btn-app-add" title="Добавить приложение">
        <span class="glyphicon glyphicon-plus"></span>
    </button>
    <h5 id="comment-context-<?=$apps[0]->id?>"><?=$apps[0]->comment?></h5>
</h3>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Приоритет</th>
            <th>Команда</th>
            <th>Параметры</th>
            <th class="text-center">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php $count=0;
            $amountApps=count($apps);
            foreach ($apps as $app) { ?>
            <tr>
                <td style="width: 50px"><?=$app->priority?></td>
                <td style="width: 120px"><?=$app->app?></td>
                <td><?=$app->appdata?></td>
                <td class="text-center" style="width: 200px">
                    <button type="button" id="btn_app_edit_<?=$app->id?>" class="btn btn-success btn-small btn-app-edit" title="Редактировать">
                        <span class="glyphicon glyphicon-ok"></span>
                    </button>
                    <button type="button" id="btn_app_priority_up_<?=$app->id?>" class="btn btn-default btn-small btn-app-priority-up <?=$count==0?'disabled':''?>" title="Поднять приоритет">
                        <span class="glyphicon glyphicon-arrow-up"></span>
                    </button>
                    <button type="button" id="btn_app_priority_down_<?=$app->id?>" class="btn btn-default btn-small btn-app-priority-down <?=$count==$amountApps-1?'disabled':''?>" title="Опустить приоритет">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </button>
                    <button type="button" id="btn_app_remove_<?=$app->id?>" class="btn btn-danger btn-small btn-app-remove" title="Удалить">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </td>
            </tr>
        <?php $count++;} ?>
    </tbody>
</table>
<?php } ?>
