<?php
    $this->breadcrumbs=array(
        'Extensions',
    );
?>

<h1>План звонков</h1>

<ul class="nav nav-tabs " id="tab_conts">
    <?php
        foreach($contexts as $context => $extens){
            $active = ($context == $curContext ? ' active' : '');
        ?>
        <li class="dropdown<?=$active?>"  >
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$context?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                <?php foreach($extens as $key=>$exten) {
                    $activeClass=($key==$curExten?'class="active"':'');
                ?>
                    <li <?=$activeClass?> ><a href="#<?=$context?>_<?=$key?>" data-toggle="tab"><?=$exten?></a></li>
                <?php } ?>
            </ul>
        </li>
    <?php } ?>
</ul>

<div class="tab-content">
    <?php foreach($contexts as $context=>$extens){
            $active = ($context == $curContext ? ' active' : '');
        ?>
        <?php foreach($extens as $key => $exten) {
            $activeExten = ($key == $curExten ? ' active' : '');
        ?>
            <div class="tab-pane<?=$activeExten?>" id="<?=$context?>_<?=$key?>" ></div>
        <?php } ?>
    <?php } ?>
</div>

<span class="pull-right">
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Расширения <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#" id="btn-show-add-exten">Добавить</a></li>
            <li><a href="#" id="btn-show-update-exten">Редактировать</a></li>
            <li class="divider"></li>
            <li><a href="#" id="btn-remove-exten">Удалить</a></li>
        </ul>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Контексты <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#" id="btn-show-add-context" >Добавить</a></li>
            <li><a href="#" id="btn-show-update-context">Редактировать</a></li>
            <li class="divider"></li>
            <li><a href="#" id="btn-remove-context">Удалить</a></li>
        </ul>
    </div>
</span>

<div id="form_edit_app" class="modal fade"></div>
<div id="form_add_app" class="modal fade"></div>

<div id="form-add-exten" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавить расширение</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="warn-add-exten">
                    <span id="span-add-exten"></span>
                </div>
                <form id="add-ext-form">
                    <fieldset>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="add-or-upd-exten" value="" />
                            <label class="control-label" for="add-exten">Расширение*</label>
                            <input type="text" class="form-control" id="add-exten" value="" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-add-exten">
                    Сохранить
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="form-add-context" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавить расширение</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="warn-add-context">
                    <span id="span-add-context"></span>
                </div>
                <form id="add-ext-form">
                    <fieldset>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="add-or-upd-context" value="" />
                            <label class="control-label" for="add-context-contxt">Контекст*</label>
                            <input type="text" class="form-control" id="add-context-context" value="" />
                        </div>
                        <div class="form-group" id="form-group-exten">
                            <label class="control-label" for="add-context-exten">Расширение*</label>
                            <input type="text" class="form-control" id="add-context-exten" value="" />
                        </div>
                        <div class="form-group" id="form-group-exten">
                            <label class="control-label" for="add-context-comment">Комментарий</label>
                            <input type="text" maxlength="256" class="form-control" id="add-context-comment" value="" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-add-context">
                    Сохранить
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="form-confirm" class="modal fade modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="btn-confirm-cancel" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirm-title">Confirm</h4>
            </div>
            <div class="modal-body" id="confirm-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-yes">Да</button>
                <button type="button" class="btn btn-primary" id="btn-confirm-no">Нет</button>
            </div>
        </div>
    </div>
</div>
<div style="height:450px" ></div>
<script>
    $(document).ready(function(){
        var curExten = <?=$curExten?>;
        var curHref = "<?=$curContext?>_<?=$curExten?>";
        var urlApps = "/extensions/apps/id/";
        var curExtenName = getExtenNameById(curExten);

        $('#tab_cons a').click(function(e){
            e.preventDefault();
            $(this).tab('show');
        });

        function getExtenNameById(id){
            var name;
            $.ajax({
                url:'/extensions/getExtenNameById/id/'+id,
                async: false,
                success: function(data){
                    try{
                        obj = JSON.parse(data);
                        if (obj.result == 1){
                            name = obj.name;
                        }else
                            name = null;
                    }catch (err){
                        name=null;
                    }
                }
            });

            return name;
        }

        function getIdExtenByElemId(id)
        {
            return id.substr(id.lastIndexOf('_') + 1);
        }

        function changePriorityApp(id, up){
            $.ajax({
                url: '/extensions/changePriority/exten/' + curExten + '/id/' + id + '/up/' + up,
                async: false,
                dataType: "html",
                success: function(data){
                    $(location).attr('href','/extensions/index/id/'+curExten);
                }
            });
        }

        function showEditModal(id){
            $.ajax({
                url: '/extensions/updateApp/exten/' + curExten + '/id/' + id,
                async: false,
                dataType: "html",
                success: function(content){
                    $('#form_edit_app').html(content)
                        .modal('show');
                }
            });
        }

        function addApp(exten){
            $.ajax({
                url: '/extensions/addApp/exten/' + exten,
                async: false,
                dataType: "html",
                success: function(content){
                    $('#form_add_app').html(content)
                        .modal('show');
                }
            });
        }

        function removeApp(id)
        {
            if (confirm("Вы действительно жедаете удалить команду")){
                $.ajax({
                    url: '/extensions/removeApp/id/'+id,
                    async: false,
                    success: function(content){
                        try{
                            obj = JSON.parse(content);
                            if ((obj.result == 1) && (obj.context > 0)){
                                $(location).attr('href', '/extensions/index/id/' + obj.context);
                            }
                        }catch(err){
                            $(location).attr('href', '/extensions/index');
                        }
                    }
                });
            }
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            try{
                curHref = $(e.target).attr('href').substr(1);
                curExten = curHref.substr(curHref.lastIndexOf('_') + 1);
                curExtenName = getExtenNameById(curExten);

                $.ajax({
                    url: urlApps + curExten,
                    async: false,
                    success: function(content){
                        $('#'+curHref).html(content);

                        $('.btn-app-edit').bind('click',function(){
                            extenId = getIdExtenByElemId($(this).attr('id'));
                            showEditModal(extenId);
                        });

                        $('.btn-app-priority-up').bind('click',function(){
                            extenId = getIdExtenByElemId($(this).attr('id'));
                            changePriorityApp(extenId, -1);
                        });

                        $('.btn-app-priority-down').bind('click',function(){
                            extenId = getIdExtenByElemId($(this).attr('id'));
                            changePriorityApp(extenId, 1)
                        });

                        $('.btn-app-add').bind('click', function(){
                            addApp(curExten);
                        });

                        $('.btn-app-remove').bind('click',function(){
                            extenId = getIdExtenByElemId($(this).attr('id'));
                            removeApp(extenId);
                        });
                    }
                });
            }catch (err){}
        });

        $('#form_edit_app').modal({show:false});
        $('#form_add_app').modal({show:false});
        $('#form-add-exten').modal({show:false});
        $('#form-add-context').modal({show:false});
        $('#form-confirm').modal({show:false});

        $('#btn-show-add-exten').click(function(){
            $('#form-add-exten .modal-header h4').html("Добавить расширение к контексту <b>" + curHref.substr(0,curHref.lastIndexOf('_'))+'</b>');
            $('#add-or-upd-exten').attr('value','add');
            $('#add-exten').attr('value','');
            $('#warn-add-exten').hide();
            $('#form-add-exten').modal('show');
        });

        $('#btn-show-update-exten').click(function(){
            $('#form-add-exten .modal-header h4').html("Редактировать расширение <b>"+curExtenName+"</b> контекста <b>"+curHref.substr(0,curHref.lastIndexOf('_'))+'</b>');
            $('#add-or-upd-exten').attr('value','upd');
            $('#add-exten').attr('value',curExtenName);
            $('#warn-add-exten').hide();
            $('#form-add-exten').modal('show');
        });

        $('#btn-show-add-context').click(function(){
            $('#form-add-context .modal-header h4').html("Добавить контекст");
            $('#add-or-upd-context').attr('value','add');
            $('#add-context-context').attr('value','');
            $('#add-context-exten').attr('value','');
            $('#form-group-exten').show();
            $('#warn-add-context').hide();
            $('#form-add-context').modal('show');
        });

        $('#btn-show-update-context').click(function(){
            $('#form-add-context .modal-header h4').html("Редактировать контекст <b>"+curHref.substr(0,curHref.lastIndexOf('_'))+'</b>');
            $('#add-or-upd-context').attr('value','upd');
            var curContext=curHref.substr(0,curHref.lastIndexOf('_'));
            var comment=$('#comment-context-'+curExten).html();
            $('#add-context-context').attr('value',curContext);
            $('#add-context-comment').attr('value',comment);
            $('#form-group-exten').hide();
            $('#warn-add-context').hide();
            $('#form-add-context').modal('show');
        });

        $('#btn-add-exten').click(function(){
            var name = $('#add-exten').val();
            var action = $('#add-or-upd-exten').val();
            if (action == 'add'){
                $.ajax({
                    url:'/extensions/addExten/exten/'+curExten+'/name/'+name,
                    success:function(data){
                        try{
                            obj=JSON.parse(data);
                            if (obj.result==0){
                                $('#span-add-exten').html(obj.error);
                                $('#warn-add-exten').show();
                            }else{
                                $(location).attr('href','/extensions/index/id/'+obj.exten);
                            }
                        }catch (err){}
                    },
                    async:false
                });
            }

            if(action=='upd'){
                $.ajax({
                    url:'/extensions/updateExten/exten/'+curExten+'/name/'+name,
                    async:false,
                    success:function(data){
                        try{
                            obj=JSON.parse(data);
                            if (obj.result == 0){
                                $('#span-add-exten').html(obj.error);
                                $('#warn-add-exten').show();
                            }else{
                                $('#warn-add-exten').hide();
                                curExtenName = name;
                                $(location).attr('href', '/extensions/index/id/' + curExten);
                            }
                        }catch (err){}
                    }
                });
            }
        });

        $('#btn-add-context').click(function(){
            var context = $('#add-context-context').val();
            var exten = $('#add-context-exten').val();
            var comment = $('#add-context-comment').val();
            var action = $('#add-or-upd-context').val();

            if (action == 'add'){
                 $.ajax({
                     url: '/extensions/addContext/context/' + context + '/exten/' + exten + '/comment/' + comment,
                     async: false,
                     success: function(data){
                         try{
                             obj = JSON.parse(data);
                             if (obj.result == 0){
                                 $('#span-add-context').html(obj.error);
                                 $('#warn-add-context').show();
                             }else{
                                $(location).attr('href', '/extensions/index/id/' + obj.exten);
                             }
                         }catch (err){}
                     }
                 });
             }

             if(action == 'upd'){
                 $.ajax({
                     url: '/extensions/updateContext/exten/' + curExten + '/context/' + context + '/comment/' + comment,
                     async: false,
                     success:function(data){
                         try{
                             obj = JSON.parse(data);
                             if (obj.result == 0){
                                 $('#span-add-context').html(obj.error);
                                 $('#warn-add-context').show();
                             }else{
                                 $('#warn-add-context').hide();
                                 $(location).attr('href', '/extensions/index/id/' + curExten);
                             }
                         }catch (err){}
                     }
                 });
             }
        });

        function modalConfirm(caption,question,callback)
        {
            $('#confirm-title').html(caption);
            $('#confirm-body').html(question);
            $('#form-confirm').modal('show');

            $('#btn-confirm-no').click(function () {
                $('#form-confirm').modal('hide');
                callback(0);
            });

            $('#btn-confirm-cancel').click(function () {
                $('#form-confirm').modal('hide');
                callback(-1);
            });

            $('#btn-confirm-yes').click(function(){
                $('#form-confirm').modal('hide');
                callback(1);
            });
        }

        $('#btn-remove-exten').click(function(){
            modalConfirm("Gamma-asterisk","Хотите удалить расширение?",function(result){
                if (result==1){
                    $.ajax({
                        url:'/extensions/removeExten/exten/'+curExten,
                        success:function(data){
                            try{
                                obj=JSON.parse(data);
                                if (obj.result==0){
                                    $('#span-add-exten').html(obj.error);
                                    $('#warn-add-exten').show();
                                }else{
                                    $('#warn-add-exten').hide();
                                    $(location).attr('href','/extensions/index/');
                                }
                            }catch (err){}
                        },
                        async:false
                    });
                }
            })
        });

        $('#btn-remove-context').click(function(){
            modalConfirm("Gamma-asterisk","Хотите удалить контекст?",function(result){
                if (result==1){
                    $.ajax({
                        url:'/extensions/removeContext/exten/'+curExten,
                        success:function(data){
                            try{
                                obj=JSON.parse(data);
                                if (obj.result==0){
                                    $('#span-add-exten').html(obj.error);
                                    $('#warn-add-exten').show();
                                }else{
                                    $('#warn-add-exten').hide();
                                    $(location).attr('href','/extensions/index');
                                }
                            }catch (err){}
                        },
                        async:false
                    });
                }
            })
        });

        $('#form_edit_app').on('hidden.bs.modal', function () {
            $('#form_edit_app').html('');
        });

        $('#form_add_app').on('hidden.bs.modal', function () {
            $('#form_add_app').html('');
        });

        try{
            $.get(urlApps+curExten,
                function(content){
                    $('#'+curHref).html(content);

                    $('.btn-app-edit').bind('click',function(){
                        btnId=$(this).attr("id");
                        curId=btnId.substr(btnId.lastIndexOf('_')+1);
                        showEditModal(curId);
                    });

                    $('.btn-app-priority-up').bind('click',function(){
                        btnId=$(this).attr("id");
                        curId=btnId.substr(btnId.lastIndexOf('_')+1);
                        changePriorityApp(curId,-1);
                    });

                    $('.btn-app-priority-down').bind('click',function(){
                        btnId=$(this).attr("id");
                        curId=btnId.substr(btnId.lastIndexOf('_')+1);
                        changePriorityApp(curId,1)
                    });

                    $('.btn-app-add').bind('click',function(){
                        addApp(curExten);
                    });

                    $('.btn-app-remove').bind('click',function(){
                        btnId=$(this).attr("id");
                        curId=btnId.substr(btnId.lastIndexOf('_')+1);
                        removeApp(curId);
                    });
                }
            )
        }catch (err){
            alert(err);
        }
    });
</script>


