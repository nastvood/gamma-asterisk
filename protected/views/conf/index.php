<div class="col-lg-offset-2 col-lg-8">
    <h1>
        <?=$fileName?>
        <button type="button" id="btn-section-add-<?=$line['section']?>" class="btn btn-primary btn-small btn-section-add" title="Добавить секцию">
            <span class="glyphicon glyphicon-plus"></span>
        </button>
    </h1>

    <table class="table table-striped table-bordered table-hover table-condensed">
        <tbody>
            <?php
                $curSection='';
                foreach($content as $line) {
                    if ($curSection!=$line['section']){
                        $curSection=$line['section']; ?>
                        <tr>
                            <td colspan="5">
                                <b><?=$line['section']?></b>
                                <button type="button" id="btn-remove-section-<?=$line['section']?>" class="btn btn-danger btn-small btn-dahdi-section-remove" title="Удалить секцию">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($line['key']){ ?>
                        <tr>
                            <td  title="<?=$line['section']?>" id="conf-pos-<?=$line['pos']?>-<?=$line['section']?>"><?=$line['pos']+1?></td>
                            <td id="conf-key-<?=$line['pos']?>-<?=$line['section']?>"><?=$line['key']?></td>
                            <td id="conf-equal-<?=$line['pos']?>-<?=$line['section']?>"><?=$line['equal_sign']?></td>
                            <td id="conf-value-<?=$line['pos']?>-<?=$line['section']?>"><?=$line['value']?></td>
                            <td class="text-center" style="width: 150px">
                                <button type="button" id="btn-conf-edit-<?=$line['pos']?>-<?=$line['section']?>" class="btn btn-success btn-xs btn-conf-edit" title="Редактировать">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </button>
                                <button type="button" id="btn-conf-add-above-<?=$line['pos']?>-<?=$line['section']?>" class="btn btn-default btn-xs btn-conf-add-above" title="Добавить выше">
                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                </button>
                                <button type="button" id="btn-conf-add-below-<?=$line['pos']?>-<?=$line['section']?>" class="btn btn-default btn-xs btn-conf-add-below" title="Добавить ниже">
                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                </button>
                                <button type="button" id="btn-conf-remove-<?=$line['pos']?>-<?=$line['section']?>-<?=$line['pos']?>" class="btn btn-danger btn-xs btn-conf-remove" title="Удалить">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>

<div id="form-edit" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="title-form-edit"></h4>
            </div>
            <div class="modal-body">
                <!--<div class="alert alert-danger" id="warn-add-exten">
                    <span id="span-add-exten"></span>
                </div>-->
                <form id="form-edit-conf" class="form-horizontal" role="form">
                    <div class="form-group">
                        <input type="hidden" class="form-control right" id="form-edit-section" value="" />
                        <input type="hidden" class="form-control right" id="form-edit-pos" value="" />
                        <div class="col-lg-5">
                            <input type="text" class="form-control" id="form-edit-key" value="" />
                        </div>
                        <div class="col-lg-7">
                            <div class="col-lg-4">
                                <select class="form-control" id="form-edit-equal">
                                    <?php $equalSigns=EqualSignConf::getList();?>
                                    <?php foreach($equalSigns as $key=>$equalSign) { ; ?>
                                        <option value="<?=$key?>"><?=$equalSign?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="form-edit-value" value="" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-edit">
                    Сохранить
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="form-confirm" class="modal fade modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="btn-confirm-cancel" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirm-title">Confirm</h4>
            </div>
            <div class="modal-body" id="confirm-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-yes">Да</button>
                <button type="button" class="btn btn-primary" id="btn-confirm-no">Нет</button>
            </div>
        </div>
    </div>
</div>

<?php
?>

<script>
    $(document).ready(function(){
        var updateSetting=true;

        $('#form-confirm').modal({show:false});
        $('#form-edit').modal({show:false});

        function modalConfirm(caption,question,callback)
        {
            $('#confirm-title').html(caption);
            $('#confirm-body').html(question);
            $('#form-confirm').modal('show');

            $('#btn-confirm-no').click(function () {
                $('#form-confirm').modal('hide');
                callback(0);
            })

            $('#btn-confirm-cancel').click(function () {
                $('#form-confirm').modal('hide');
                callback(-1);
            })

            $('#btn-confirm-yes').click(function(){
                $('#form-confirm').modal('hide');
                callback(1);
            });
        }

        $('.btn-conf-edit').bind('click',function(){
            updateSetting=true;
            var idElem=$(this).attr('id');
            var idPos=$('#conf-pos-'+idElem.replace('btn-conf-edit-','')).attr('id');

            var section=$('#'+idPos).attr('title');
            var pos=$('#'+idPos).html();
            var key=$('#conf-key-'+idElem.replace('btn-conf-edit-','')).html();
            var value=$('#conf-value-'+idElem.replace('btn-conf-edit-','')).html();
            var equal=$('#conf-equal-'+idElem.replace('btn-conf-edit-','')).html();

            $('#title-form-edit').html('Редактировать:');
            $('#form-edit-section').val(section);
            $('#form-edit-pos').val(pos-1);
            $('#form-edit-key').val(key);
            $('#form-edit-value').val(value);
            var equalVal=$('#form-edit-equal option').filter(function(){
                return $(this).html()==equal;
            }).val();
            $('#form-edit-equal').val(equalVal);

            $('#form-edit').modal('show');
        });

        $('.btn-conf-add-above').bind('click',function(){
            updateSetting=false;
            var idElem=$(this).attr('id');
            var idPos=$('#conf-pos-'+idElem.replace('btn-conf-add-above-','')).attr('id');

            var pos=$('#'+idPos).html();

            $('#title-form-edit').html('Добавить:');
            $('#form-edit-section').val('');
            $('#form-edit-pos').val(parseInt(pos)-1);
            $('#form-edit-key').val('');
            $('#form-edit-value').val('');
            $('#form-edit-equal').val(0);

            $('#form-edit').modal('show');
        });

        $('.btn-conf-add-below').bind('click',function(){
            updateSetting=false;
            var idElem=$(this).attr('id');
            var idPos=$('#conf-pos-'+idElem.replace('btn-conf-add-below-','')).attr('id');

            var pos=$('#'+idPos).html();

            $('#title-form-edit').html('Добавить:');
            $('#form-edit-section').val('');
            $('#form-edit-pos').val(parseInt(pos));
            $('#form-edit-key').val('');
            $('#form-edit-value').val('');
            $('#form-edit-equal').val(0);

            $('#form-edit').modal('show');
        });

        $('.btn-conf-remove').bind('click',function(){
            var idElem=$(this).attr('id');
            var idPos=$('#conf-pos-'+idElem.replace('btn-conf-add-below-','')).attr('id');

            var pos=$('#'+idPos).html();

            modalConfirm("Gamma-asterisk","Хотите удалить директиву настройки?",function(result){
                if (result==1){
                    /*$('#title-form-edit').html('Добавить:');
                     $('#form-edit-section').val('');
                     $('#form-edit-pos').val(parseInt(pos)-1);
                     $('#form-edit-key').val('');
                     $('#form-edit-value').val('');
                     $('#form-edit-equal').val(0);*/
                }
            })
        });

        $("#btn-edit").click(function(){
            var section=$('#form-edit-section').val();
            var key=$('#form-edit-key').val();
            var equal=$('#form-edit-equal').val();
            var value=$('#form-edit-value').val();
            var pos=$('#form-edit-pos').val();

            var script='/dahdi/updatesetting';
            if (!updateSetting){
                script='/dahdi/addsetting';
            }

            var url='/dahdi/'+updateSetting+'/section/'+section+'/key/'+key+'/equal/'+equal+'/value/'+value+'/pos/'+pos;
            //console.log(url);
            $.ajax({
                url:url,
                async:false,
                success:function(){
                    $(location).attr('href','/dahdi');
                }
            });
        });
    });
</script>
