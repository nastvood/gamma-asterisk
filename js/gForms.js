var getEventCount = function(elem,event){
    if (typeof $._data(elem.get(0),'events') === 'undefined')
        return 0;

    if (typeof $._data(elem.get(0),'events')[event] === 'undefined')
        return 0;

    return $._data(elem.get(0),'events')[event].length;
};

var gMessages = (function(){
    var WARNING = 'warning';
    var INFO = 'info';
    var SUCCESS = 'success';
    var DANGER = 'danger';

    var showMessage = function(title,mess,type){
        var alert = '#modal-message .alert';
        var header = '#modal-message .modal-header';

        $(alert).attr('class','alert alert-'+type+' alert-dismissable')
            .empty()
            .append('<span>'+mess+'</span>');

        $(header).empty()
            .append('<span><b>'+title+'</b></span>');

        $('#modal-message').modal('show');
    };

    return {
        showWarning : function(title,mess){
            showMessage(title,mess,WARNING);
        },
        showInfo : function(title,mess){
            showMessage(title,mess,INFO);
        },
        showSuccess : function(title,mess){
            showMessage(title,mess,SUCCESS);
        },

        showDanger : function(title,mess){
            showMessage(title,mess,DANGER);
        }
    }
})();

var gConfirmMessage = (function(){
    var self=this;

    var alertClass='alert alert-warning alert-dismissable';
    var modalSelector='#modal-confirm';
    var alertSelector=modalSelector+' .alert';
    var headerSelector=modalSelector+' .modal-header';
    var btnNoSelector=modalSelector+'-btn-no';
    var btnYesSelector=modalSelector+'-btn-yes';
    var resultClick=false;
    var response=function(){};

    $(modalSelector).modal({
        backdrop:'static',
        show:false
    });

    $(modalSelector).hide();

    var btnNoClick=function(e){
        response(-1);
        $(modalSelector).modal('hide');
    }

    var btnYesClick=function(e){
        response(1);
        $(modalSelector).modal('hide');
    }

    return {
        confirm : function(title,mess,rescallback){
            response=rescallback;

            if (getEventCount($(btnNoSelector),'click'))
                $(btnNoSelector).off('click',btnNoClick);

            if (getEventCount($(btnNoSelector),'click'))
                $(btnNoSelector).off('click',btnNoClick);

            $(btnNoSelector).on('click',btnNoClick);
            $(btnYesSelector).on('click',btnYesClick);

            $(alertSelector).empty();
            $(headerSelector).empty();
            $(modalSelector).modal('show');
            $(headerSelector).append('<span><b>'+title+'</b></span>');
            $(alertSelector).append('<span>'+mess+'</span>');
        }
    }
})();

function YiiForm(modalId,modelName,checkUrlCallback,saveUrlCallback){
    var modalIdSelector='#'+modalId;
    var formId='form-'+modalId;
    var formFormSelector='#'+formId;
    var formInputsSelector=formFormSelector+' input';
    var formOptionsSelector=formFormSelector+' option:selected';
    var formSelectSelector=formFormSelector+' select';
    var formAlertErrorsSelector=formFormSelector+' .modal-errors';
    var formAlertSelector=formFormSelector+' .alert';
    var formTitleSelector=modalIdSelector+' .modal-title';
    var formInputs={};
    var errorCounter=0;
    var errorsInput=[];
    var saveMessage='';
    var saveId=0;
    var saveData={};

    $(formAlertSelector).hide();

    var prepareData=function(){
        var ajaxData={};

        for(var inputName in formInputs){
            ajaxData[inputName] = formInputs[inputName].value;
        }

        return ajaxData;
    };

    var getData = function(){
        $(formInputsSelector).each(function(){
            var key=modelName+'['+this.name+']';
            formInputs[key].value=this.value;
        });

        $(formOptionsSelector).each(function(){
            var name=$(this).parent().attr('name');
            var key=modelName+'['+name+']';
            formInputs[key].value=$(this).text();
        });

        return prepareData();
    };

    var setFocused = function(nameInput){
        var key=modelName+'['+nameInput+']';
        formInputs[key]['focused']=true;
    };

    var clearFocused = function(){
        for(var inputName in formInputs){
            formInputs[inputName].focused=false;
        }
    };

    var errInputToName = function(errInputName){
        var pos = errInputName.indexOf('_');
        if (pos>-1)
            return errInputName.substr(pos+1);
        else
            return errInputName;
    };

    var isFocused = function(nameInput){
        var key = modelName+'['+nameInput+']';
        return formInputs[key].focused;
    };

    var checkUrlHandler = function(errors){
        for(var name in errors){
            if (isFocused(errInputToName(name))){
                for (var error in errors[name]){
                    $(formAlertErrorsSelector).append('<span>'+errors[name][error]+'</span><br>');
                    errorCounter++;
                }
            }
        }

        if (errorCounter)
            $(formAlertSelector).show();
        else
            $(formAlertSelector).hide();

    };

    var checkAllUrlHandler = function(errors){
        for(var name in errors){
            for (var error in errors[name]){
                $(formAlertErrorsSelector).append('<span>'+errors[name][error]+'</span><br>');
                errorCounter++;
            }
        }

        if (errorCounter)
            $(formAlertSelector).show();
        else
            $(formAlertSelector).hide();

    };

    var formInputsFocusOut = function(e){
        $(formAlertErrorsSelector).empty();
        errorCounter=0;

        if ($(e.relatedTarget).prop('tagName')== 'BUTTON')
            return false;

        setFocused(this.name);

        var nameInput=this.name;

        var postData=getData();
        postData.ajax=true;

        $.post(
            checkUrlCallback(),
            postData,
            checkUrlHandler
        );

        return false;
    };

    var formInputsFocusIn = function(){};

    $(formInputsSelector).each(function(){
        var key=modelName+'['+this.name+']';
        formInputs[key]={value:'',focused:false};
    });

    $(formSelectSelector).each(function(){
        var key=modelName+'['+this.name+']';
        formInputs[key]={value:'',focused:false};
    });

    return {
        getSaveMessage : function(){
            return saveMessage;
        },

        getSaveId : function(){
            return saveId;
        },

        getSaveData : function(){
            return saveData;
        },

        setTitle : function(title){
            $(formTitleSelector).html(title);
        },

        open : function(){
            checkUrlCallback && $(formInputsSelector).on('focusout',formInputsFocusOut);
            checkUrlCallback && $(formInputsSelector).on('focusin',formInputsFocusIn);

            clearFocused();

            $(modalIdSelector).modal('show');
        },

        close : function(){
            $(formInputsSelector).off('focusout');
            $(formInputsSelector).off('focusin');

            $(modalIdSelector).modal('hide');
        },

        save : function(){
            $(formAlertErrorsSelector).hide();
            $(formAlertErrorsSelector).empty();
            $(formAlertSelector).hide();

            var postData=getData();
            postData.ajax=true;

            errorCounter=0;

            $.ajax({
                type:'POST',
                url:checkUrlCallback(),
                data:postData,
                success:checkAllUrlHandler,
                dataType:'json',
                async:false
            });

            if (errorCounter)
                return false;

            var result=true;
            $.ajax({
                url:saveUrlCallback(),
                data:getData(),
                success:function(data){
                    try{
                        saveMessage=data.msg;
                        saveId=data.id;
                        saveData=data;
                        $(formAlertErrorsSelector).empty();

                        if (data.err){
                            result=false;
                            if (typeof data.errMsg==='object'){
                                for(var name in data.errMsg){
                                    $(self.formAlertSelector).show();
                                    for(var err in data.errMsg[name]){
                                        saveMessage+='<span>'+data.errMsg[name][err]+'</span><br>';
                                        $(formAlertErrorsSelector).append('<span>'+data.errMsg[name][err]+'</span><br>');
                                    }
                                }
                            }else{
                                $(formAlertSelector).show();
                                $(formAlertErrorsSelector).append('<span>'+data.errMsg+'</span><br>');
                                saveMessage+='<span>'+data.errMsg+'</span><br>';
                            }
                        }
                    }catch(e){
                        saveMessage='';
                        saveId=0;
                        result=false;
                        $(formAlertErrorsSelector).append('<span>Неизвестная ошибка</span><br>');
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){
                    saveMessage=xhr.responseText;
                    saveId=0;
                    result=false;
                    $(formAlertSelector).show();
                    $(formAlertErrorsSelector).append('<span>'+xhr.responseText+'</span><br>');
                },
                type:'POST',
                dataType:'json',
                async:false
            });

            return result;
        }
    }
}
